# Assumptions being made:

- Diseases
    - Each disease can be uniquely identified by its name
- Symptoms
    - Symptoms and signs are uniquely identified by
- Questions and Answers are strong entities
    - We have assigned a unique identifier to each question and answer


# TODO

- Cardinality Check
- Full participation
- Primary Keys
- Weak entities
- Extended ER
- Identifying relationship


# Explicit Objects: (put all these in the ER?)

- Home users
    - Attributes
        - loginx
        - contact detailsx
            - phone(mv)
            - email address
            - address
            - city
            - street
            - zip
        - personal detailsx
            - name
            - first
            - middle
            - last
        - current medication/therapy
            - dosage
            - type
        - allergies
    - Relationships with other entities
        - Disease
            - User has had a Disease
        - OnlineExpert
            - User has been designated a single OnlineExpert
        - Complaint(weak)
            - User filed a complaint
        - Diagnosis(weak)
            - A diagnosis has been made for user
        - Symptoms(weak)
            - User reports symptoms
        - Diagnostic Test
            - prescribed to
                - home-collectable diagnostic-results(weak)?
        - Question(weak)
            - User asks
                - healthcare questions
- Online experts
    - Login
    - Contact details
    - Professional credentials
    - role
        - clinicians
        - specialists
        - general practitioners
        - therapists
        - medical/healthcare consultants
- Signs and symptoms
    - timestamp
    - severity
    - worded description
- Diagnoses
    - timestamp
    - worded description
    - disease
- Diseases
    - type
    - name
    - worded description
- Home-collectable diagnostic tests
    - timestamp
    - worded description
    - result
        - positive
        - negative
    - test info (appendix A)
        - condition
        - test process
        - tool name
        - method
- Treatments
    - timestamp
    - type
    - name
    - worded description
- Healthcare questions
    - timestamp
    - type
        - private
        - open
    - title
    - text
- Healthcare answers
    - timestamp
    - type
        - private
        - open
    - text

# Context DataFlow Diagram

- Identify:
    - I/O of the system
    - Processes in the system
    - Process logic of the system
- Components:
    - Terminator: source or sink of information
    - Data Flow: carries data from one place in the system to another at least one end must be a process (otherwise it's not part of the system)
    - Process: activity that occurs in the system must have both input and output
    - Data Store: information repository
        - connects only to processes
        - must have both input and output
- Terminators:
    - Home users: people who are being monitored
    - Online experts: people who provide consultation services
- Inputs:
    - Home users:
        - personal information
        - signs and symptoms
        - home-collectable diagnostic tests
        - healthcare questions
    - Online experts:
        - personal information
        - signs and symptoms
        - diagnoses
        - diseases
        - treatments
        - healthcare answers
- Data Stores:
    - Personal information
    - Signs and symptoms
    - Diagnoses
    - Home-collectable diagnostic tests
    - Treatments
    - Healthcare questions
    - Healthcare answers
- Processes:
    - Registration
    - Diagnosis
    - Questions

# Entity Relationship Diagram

- Entity: something with attributes
- Relationship: how one entity is related to another
- Attribute: a piece of information about an entity or relationship

# Mapping

- Map ERD to relations and keys

# Normalization

- Checking relations and keys for redundancy and other things
