# Mapping

- $ = key
- ^ = foreign key


# Entities

- USERS($login, password)
- HOME_USERS($login^, expert^, personal_details, contact_details)
- ONLINE_EXPERTS($login^, contact_details)
- ANSWERS($no, $question_no^, $asked_by^, given_by^, timestamp, privacy, text)
- COMPLAINTS($no, $made_by^, timestamp, text)
- DIAGNOSES($no, $for^, $by^, disease^, timestamp, description)
- DISEASES($name, type, description)
- OPINIONS($no, $symptom^, $experienced_by^, made_by^, text)
- QUESTIONS($no, $asked_by^, timestamp, privacy, text)
- SYMPTOMS($no, $experienced_by^, reported_by^, timestamp, severity, description)
- TEST_REQUESTS($no, $for^, requested_by^, test^, timestamp, positive, description)
- TESTS($no, condition, process, tool, method)
- TREATMENTS($no, $for^, $prescribed_by^, type, name, description)


# Multivalue attributes

- ALLERGIES($home_user^, $allergy)
- CURRENTMEDICATIONS($home_user^, medications)
- CREDENTIALS($expert^, $credentials)
- ROLES($expert^, $role)


# Relationships

- DISEASE_HISTORY($no, $home_user^, $disease^, timestamp)


# Foreign keys

- HOME_USERS.login -> USERS.login
- HOME_USERS.expert -> ONLINE_EXPERTS.login
- ONLINE_EXPERTS.login -> USERS.login
- ANSWERS.question_no -> QUESTIONS.no
- ANSWERS.asked_by -> QUESTIONS.asked_by
- ANSWERS.given_by -> ONLINE_EXPERTS.login
- COMPLAINTS.made_by -> HOME_USERS.login
- DIAGNOSES.for -> HOME_USERS.login
- DIAGNOSES.by -> ONLINE_EXPERTS.login
- DIAGNOSES.disease -> DISEASES.name
- OPINIONS.symptom -> SYMPTOMS.no
- OPINIONS.experienced_by -> SYMPTOMS.experienced_by
- QUESTIONS.asked_by -> HOME_USERS.login
- SYMPTOMS.experienced_by -> HOME_USERS.login
- SYMPTOMS.reported_by -> USERS.login
- TEST_REQUESTS.for -> HOME_USERS.login
- TEST_REQUESTS.requested_by -> ONLINE_EXPERTS.login
- TEST_REQUESTS.test -> TESTS.no
- TREATMENTS.for -> HOME_USERS.login
- TREATMENTS.prescribed_by -> ONLINE_EXPERTS.login
- ALLERGIES.home_user -> HOME_USERS.login
- CREDENTIALS.expert -> ONLINE_EXPERTS.login
- ROLES.expert -> ONLINE_EXPERTS.login
