# Normalisation

- $ = key
- ^ = foreign key


# Functional Dependencies

- USERS($login, password)
    - Users provide a password for a user entity, which is uniquely identified
        by:
    - {login} -> {password}
- HOME_USERS($login^, expert^, personal_details, contact_details)
    - Home users provide their personal and contact details for a user entity,
        which is uniquely identified by:
    - {login} -> {expert, personal_details, contact_details}
- ONLINE_EXPERTS($login^, contact_details)
    - Online experts provide their contact details for a user entity,
        which is uniquely identified by:
    - {login} -> {contact_details}
- ANSWERS($no, $question_no^, $asked_by^, given_by^, timestamp, private, text)
    - Online experts provide answers to questions for an answer entity, which is
        uniquely identified by:
    - {no, question_no, asked_by} -> {given_by, timestamp, private, text}
- COMPLAINTS($no, $made_by^, timestamp, text)
    - Home users provide complaint details for a complaint entity, which is
        uniquely identified by:
    - {no, made_by} -> {timestamp, text}
- DIAGNOSES($no, $for^, $by^, disease^, timestamp, description)
    - Online experts provide diagnosis details for a diagnosis entity, which is
        uniquely identified by:
    - {no, for, by} -> {disease, timestamp, description}
- DISEASES($name, type, description)
    - Disease are uniquely identified by their name
    - Multiple diseases can have the same type, though maybe not description
        (description should not be strictly unique)
    - {name} -> {type, description}
- OPINIONS($no, $sympton^, $experienced_by^, made_by^, text)
    - No constraint has been applied on the number of opinions one doctor can
        give for one symptom, so {sympton, experienced_by, made_by} does not
        need to be unique
    - Online experts provide opinions details for an opinion entity, which is
        uniquely identified by:
    - {no, sympton, experienced_by} -> {made_by, text}
- QUESTIONS($no, $asked_by^, timestamp, private, text)
    - Home users provide question details for a question entity, which is
        uniquely identified by:
    - {no, asked_by} -> {timestamp, private, text}
- SYMPTONS($no, $experienced_by^, reported_by^, timestamp, severity, description)
    - Symptoms can be entered by a home user instead of being reported by an
        online expert
    - Users provide symptom details for a symptom entity, which is uniquely
        identified by:
    - {no, experienced_by} -> {reported_by, timestamp, severity, description}
- TEST_REQUESTS($no, $for^, requested_by^, test^, timestamp, positive, description)
    - Online experts provide test request details for a test request entity,
        which is uniquely identified by:
    - {no, for} -> {requested_by, test, timestamp, positive, description}
- TESTS($no, condition, process, tool, method)
    - No test attributes are unique
    - {no} -> {condition, process, tool, method}
- TREATMENTS($no, $for^, $prescribed_by^, type, name, description)
    - Online experts provide treatment details for a treatment entity, which is
            uniquely identified by:
    - {no, for, prescribed_by} -> {type, name, description}
- ALLERGIES($home_user^, $allergy)
    - Many to many relationship table, no functional dependencies
- CREDENTIALS($expert^, $credentials)
    - Many to many relationship table, no functional dependencies
- ROLES($expert^, $role)
    - Many to many relationship table, no functional dependencies
- DISEASE_HISTORY($no, $home_user^, $disease^, timestamp)
    - Home user can have many diseases at many times
    - {no, home_user, disease} -> {timestamp}


# Normal form

- 1NF
    - All attributes are atomic
- 2NF
    - All attributes in every relation are either part of the primary key or
    - dependent on the primary key. Therefore there are no partial dependencies.
- 3NF
    - No relations have more than one functional dependency. Therefore there are
    - no transitive dependencies.
