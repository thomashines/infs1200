

-- Query 6

-- Remove a clinician and records associated with this clinician in all tables
-- from the system. You need to decide the strategy on how the Foreign Key
-- values are to be handled in the system. For example, you can replace the
-- removed Foreign Key values by NULL values (called Nullified strategy).

-- Use nullified strategy

-- ONLINE_CLINICIAN
--  DIAG_DISEASE.OID (primary key, delete rows)
--  DIAGNOSIS.OID (primary key, delete rows)
--  TEST_RECOMMENDATION.OID (primary key, delete rows)
--  ANSWER.OID
--  CONSULTATION.OID
--  HOME_USER.OID
--  TREATMENT.OID

-- Set remove_oid to the OID of the online clinician to be removed
SET @remove_oid = 3;

-- DIAG_DISEASE
DELETE FROM DIAG_DISEASE
WHERE OID = @remove_oid;

-- DIAGNOSIS
DELETE FROM DIAGNOSIS
WHERE OID = @remove_oid;

-- TEST_RECOMMENDATION
DELETE FROM TEST_RECOMMENDATION
WHERE OID = @remove_oid;

-- ANSWER
UPDATE ANSWER
SET OID = NULL
WHERE OID = @remove_oid;

-- CONSULTATION
UPDATE CONSULTATION
SET OID = NULL
WHERE OID = @remove_oid;

-- HOME_USER
UPDATE HOME_USER
SET OID = NULL
WHERE OID = @remove_oid;

-- TREATMENT
UPDATE TREATMENT
SET OID = NULL
WHERE OID = @remove_oid;

-- ONLINE_CLINICIAN
DELETE FROM ONLINE_CLINICIAN
WHERE OID = @remove_oid;

-- Output

-- [84, 21:08:32] SET @remove_oid = 3: Running...
-- [84, 21:08:32] SET @remove_oid = 3: 0 row(s) affected
-- [85, 21:08:32] DELETE FROM DIAG_DISEASE
-- WHERE OID = @remove_oid: Running...
-- [85, 21:08:32] DELETE FROM DIAG_DISEASE
-- WHERE OID = @remove_oid: 2 row(s) affected
-- [86, 21:08:32] DELETE FROM DIAGNOSIS
-- WHERE OID = @remove_oid: Running...
-- [86, 21:08:32] DELETE FROM DIAGNOSIS
-- WHERE OID = @remove_oid: 2 row(s) affected
-- [87, 21:08:32] DELETE FROM TEST_RECOMMENDATION
-- WHERE OID = @remove_oid: Running...
-- [87, 21:08:32] DELETE FROM TEST_RECOMMENDATION
-- WHERE OID = @remove_oid: 2 row(s) affected
-- [88, 21:08:32] UPDATE ANSWER
-- SET OID = NULL
-- WHERE OID = @remove_oid: Running...
-- [88, 21:08:32] UPDATE ANSWER
-- SET OID = NULL
-- WHERE OID = @remove_oid: 3 row(s) affected
-- Rows matched: 3  Changed: 3  Warnings: 0
-- [89, 21:08:32] UPDATE CONSULTATION
-- SET OID = NULL
-- WHERE OID = @remove_oid: Running...
-- [89, 21:08:32] UPDATE CONSULTATION
-- SET OID = NULL
-- WHERE OID = @remove_oid: 2 row(s) affected
-- Rows matched: 2  Changed: 2  Warnings: 0
-- [90, 21:08:32] UPDATE HOME_USER
-- SET OID = NULL
-- WHERE OID = @remove_oid: Running...
-- [90, 21:08:32] UPDATE HOME_USER
-- SET OID = NULL
-- WHERE OID = @remove_oid: 2 row(s) affected
-- Rows matched: 2  Changed: 2  Warnings: 0
-- [91, 21:08:32] UPDATE TREATMENT
-- SET OID = NULL
-- WHERE OID = @remove_oid: Running...
-- [91, 21:08:32] UPDATE TREATMENT
-- SET OID = NULL
-- WHERE OID = @remove_oid: 0 row(s) affected
-- Rows matched: 0  Changed: 0  Warnings: 0
-- [92, 21:08:32] DELETE FROM ONLINE_CLINICIAN
-- WHERE OID = @remove_oid: Running...
-- [92, 21:08:32] DELETE FROM ONLINE_CLINICIAN
-- WHERE OID = @remove_oid: 1 row(s) affected
