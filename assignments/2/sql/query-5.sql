

-- Query 5

-- Update a user record in the system. A user can be either a home user or an
-- online clinician.

-- Replace all <fields between less than and greater than signs> with the user's
-- relevant details.
-- Replace the NULL on line 23 with the OID of the home user's online clinician.

-- Select the user to update with the values on lines 25-27 and 39-41, comment
-- out unknowns.

-- Home user
UPDATE HOME_USER
SET
    Phone = "<phone no>",
    eMail = "<email address>",
    FName = "<first name>",
    LName = "<last name>",
    Address = "<address>",
    OID = NULL
WHERE 1
-- AND FName = "<first name>"
-- AND LName = "<first name>"
AND UID = 2
;

-- Online clinician
UPDATE ONLINE_CLINICIAN
SET
    Phone = "<phone no>",
    eMail = "<email address>",
    FName = "<first name>",
    LName = "<last name>",
    Address = "<address>"
WHERE 1
-- AND FName = "<first name>"
-- AND LName = "<first name>"
AND OID = 2
;

-- Output

-- [82, 21:07:08] UPDATE HOME_USER
-- SET
--     Phone = "<phone no>",
--     eMail = "<email address>",
--     FName = "<first name>",
--     LName = "<last name>",
--     Address = "<address>",
--     OID = NULL
-- WHERE 1
-- -- AND FName = "<first name>"
-- -- AND LName = "<first name>"
-- AND UID = 2: Running...
-- [82, 21:07:08] UPDATE HOME_USER
-- SET
--     Phone = "<phone no>",
--     eMail = "<email address>",
--     FName = "<first name>",
--     LName = "<last name>",
--     Address = "<address>",
--     OID = NULL
-- WHERE 1
-- -- AND FName = "<first name>"
-- -- AND LName = "<first name>"
-- AND UID = 2: 1 row(s) affected
-- Rows matched: 1  Changed: 1  Warnings: 0
-- [83, 21:07:08] UPDATE ONLINE_CLINICIAN
-- SET
--     Phone = "<phone no>",
--     eMail = "<email address>",
--     FName = "<first name>",
--     LName = "<last name>",
--     Address = "<address>"
-- WHERE 1
-- -- AND FName = "<first name>"
-- -- AND LName = "<first name>"
-- AND OID = 2: Running...
-- [83, 21:07:08] UPDATE ONLINE_CLINICIAN
-- SET
--     Phone = "<phone no>",
--     eMail = "<email address>",
--     FName = "<first name>",
--     LName = "<last name>",
--     Address = "<address>"
-- WHERE 1
-- -- AND FName = "<first name>"
-- -- AND LName = "<first name>"
-- AND OID = 2: 1 row(s) affected
-- Rows matched: 1  Changed: 1  Warnings: 0
