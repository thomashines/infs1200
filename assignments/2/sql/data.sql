

-- Sample data construction

-- # Independent tables

-- * DISEASE (ICD10CODE*, Description)
INSERT INTO DISEASE (ICD10CODE, Description)
VALUES ("L20", "Atopic dermatitis"),
       ("L21", "Seborrhoeic dermatitis"),
       ("L22", "Diaper [napkin] dermatitis"),
       ("L23", "Allergic contact dermatitis"),
       ("L24", "Irritant contact dermatitis"),
       ("L25", "Unspecified contact dermatitis"),
       ("L26", "Exfoliative dermatitis"),
       ("L27", "Dermatitis due to substances taken internally"),
       ("L28", "Lichen simplex chronicus and prurigo"),
       ("L29", "Pruritus");

-- * HCHT (TESTID*, Condition, Process, Toolname, Method)
INSERT INTO HCHT (`Condition`, Process, Toolname, Method)
VALUES ("Condition 1", "Process 1", "Toolname 1", "Method 1"),
       ("Condition 2", "Process 2", "Toolname 2", "Method 2"),
       ("Condition 3", "Process 3", "Toolname 3", "Method 3"),
       ("Condition 4", "Process 4", "Toolname 4", "Method 4"),
       ("Condition 5", "Process 5", "Toolname 5", "Method 5"),
       ("Condition 6", "Process 6", "Toolname 6", "Method 6"),
       ("Condition 7", "Process 7", "Toolname 7", "Method 7"),
       ("Condition 8", "Process 8", "Toolname 8", "Method 8"),
       ("Condition 9", "Process 9", "Toolname 9", "Method 9"),
       ("Condition 10", "Process 10", "Toolname 10", "Method 10");

-- * ONLINE_CLINICIAN (OID*, Phone, eMail, FName, LName, Address)
INSERT INTO ONLINE_CLINICIAN (Phone, eMail, FName, LName, Address)
VALUES ("1234567891", "1@online.clinician", "Dr 1", "MD 1", "Place 1"),
       ("1234567892", "2@online.clinician", "Dr 2", "MD 2", "Place 2"),
       ("1234567893", "3@online.clinician", "Dr 3", "MD 3", "Place 3"),
       ("1234567894", "4@online.clinician", "Dr 4", "MD 4", "Place 4"),
       ("1234567895", "5@online.clinician", "Dr 5", "MD 5", "Place 5"),
       ("1234567896", "6@online.clinician", "Dr 6", "MD 6", "Place 6"),
       ("1234567897", "7@online.clinician", "Dr 7", "MD 7", "Place 7"),
       ("1234567898", "8@online.clinician", "Dr 8", "MD 8", "Place 8"),
       ("1234567899", "9@online.clinician", "Dr 9", "MD 9", "Place 9"),
       ("1234567900", "10@online.clinician", "Dr 10", "MD 10", "Place 10");

-- # Level 1

-- * HOME_USER (UID*, Phone, eMail, FName, LName, Address, OID)
--  * HOME_USER.OID -> ONLINE_CLINICIAN.OID
INSERT INTO HOME_USER (Phone, eMail, FName, LName, Address, OID)
VALUES ("0987654321", "1@home.user", "Real 1", "Person 1", "Home 1", 1),
       ("0987654322", "2@home.user", "Real 2", "Person 2", "Home 2", 7),
       ("0987654323", "3@home.user", "Real 3", "Person 3", "Home 3", 3),
       ("0987654324", "4@home.user", "Real 4", "Person 4", "Home 4", 5),
       ("0987654325", "5@home.user", "Real 5", "Person 5", "Home 5", 5),
       ("0987654326", "6@home.user", "Real 6", "Person 6", "Home 6", 3),
       ("0987654327", "7@home.user", "Real 7", "Person 7", "Home 7", 7),
       ("0987654328", "8@home.user", "Real 8", "Person 8", "Home 8", 10),
       ("0987654329", "9@home.user", "Real 9", "Person 9", "Home 9", 9),
       ("0987654330", "10@home.user", "Real 10", "Person 10", "Home 10", 10);

-- # Level 2

-- * CONSULTATION (CID*, UID, OID, Time)
--  * CONSULTATION.UID -> HOME_USER.UID
--  * CONSULTATION.OID -> ONLINE_CLINICIAN.OID
INSERT INTO CONSULTATION (UID, OID, Time)
VALUES (1, 1, "2016-10-21 01:00:00"),
       (2, 2, "2016-10-21 02:00:00"),
       (3, 3, "2016-10-21 03:00:00"),
       (2, 1, "2016-10-21 04:00:00"),
       (5, 5, "2016-10-21 05:00:00"),
       (6, 7, "2016-10-21 06:00:00"),
       (8, 7, "2016-10-21 07:00:00"),
       (1, 8, "2016-10-21 08:00:00"),
       (9, 3, "2016-10-21 09:00:00"),
       (4, 2, "2016-10-21 10:00:00");

-- * DIAGNOSIS (UID*, OID*, DTimestamp*, DText)
--  * DIAGNOSIS.UID -> HOME_USER.UID
--  * DIAGNOSIS.OID -> ONLINE_CLINICIAN.OID
INSERT INTO DIAGNOSIS (UID, OID, DTimestamp, DText)
VALUES (1, 1, "2016-10-21 01:00:00", "Diagnosis 1"),
       (2, 2, "2016-10-21 02:00:00", "Diagnosis 2"),
       (3, 3, "2016-10-21 03:00:00", "Diagnosis 3"),
       (2, 1, "2016-10-21 04:00:00", "Diagnosis 4"),
       (5, 5, "2016-10-21 05:00:00", "Diagnosis 5"),
       (6, 7, "2016-10-21 06:00:00", "Diagnosis 6"),
       (8, 7, "2016-10-21 07:00:00", "Diagnosis 7"),
       (1, 8, "2016-10-21 08:00:00", "Diagnosis 8"),
       (9, 3, "2016-10-21 09:00:00", "Diagnosis 9"),
       (4, 2, "2016-10-21 10:00:00", "Diagnosis 10");

-- * QUESTION (QID*, QText, Type, QTimestamp, UID)
--  * QUESTION.UID -> HOME_USER.UID
INSERT INTO QUESTION (QText, Type, QTimestamp, UID)
VALUES ("Question 1?", "private", "2016-10-21 01:00:00", 1),
       ("Question 2?", "open", "2016-10-21 02:00:00", 2),
       ("Question 3?", "open", "2016-10-21 03:00:00", 5),
       ("Question 4?", "open", "2016-10-21 04:00:00", 9),
       ("Question 5?", "private", "2016-10-21 05:00:00", 1),
       ("Question 6?", "private", "2016-10-21 06:00:00", 2),
       ("Question 7?", "open", "2016-10-21 07:00:00", 2),
       ("Question 8?", "private", "2016-10-21 08:00:00", 3),
       ("Question 9?", "private", "2016-10-21 09:00:00", 4),
       ("Question 10?", "private", "2016-10-21 10:00:00", 5);

-- * TEST_RECOMMENDATION (UID*, OID*, Time*, Description, Type, TESTID)
--  * TEST_RECOMMENDATION.UID -> HOME_USER.UID
--  * TEST_RECOMMENDATION.OID -> ONLINE_CLINICIAN.OID
--  * TEST_RECOMMENDATION.TESTID -> HCHT.TESTID
INSERT INTO TEST_RECOMMENDATION (UID, OID, Time, Description, Type, TESTID)
VALUES (1, 7, "2016-10-21 01:00:00", "Test recommendation 1", "Type 1", 5),
       (3, 8, "2016-10-21 02:00:00", "Test recommendation 2", "Type 2", 2),
       (7, 6, "2016-10-21 03:00:00", "Test recommendation 3", "Type 3", 4),
       (4, 1, "2016-10-21 04:00:00", "Test recommendation 4", "Type 4", 6),
       (4, 3, "2016-10-21 05:00:00", "Test recommendation 5", "Type 5", 3),
       (2, 4, "2016-10-21 06:00:00", "Test recommendation 6", "Type 6", 5),
       (8, 8, "2016-10-21 07:00:00", "Test recommendation 7", "Type 7", 7),
       (8, 1, "2016-10-21 08:00:00", "Test recommendation 8", "Type 8", 9),
       (9, 3, "2016-10-21 09:00:00", "Test recommendation 9", "Type 9", 4),
       (10, 4, "2016-10-21 10:00:00", "Test recommendation 10", "Type 10", 5);

-- * TEST_RESULT (TRID*, Result, Description, Time, UID, TESTID)
--  * TEST_RESULT.UID -> HOME_USER.UID
--  * TEST_RESULT.TESTID -> HCHT.TESTID
INSERT INTO TEST_RESULT (Result, Description, Time, UID, TESTID)
VALUES ("Test result 1", "Test result description 1", "2016-10-21 01:00:00", 6, 4),
       ("Test result 2", "Test result description 2", "2016-10-21 02:00:00", 3, 6),
       ("Test result 3", "Test result description 3", "2016-10-21 03:00:00", 4, 3),
       ("Test result 4", "Test result description 4", "2016-10-21 04:00:00", 9, 4),
       ("Test result 5", "Test result description 5", "2016-10-21 05:00:00", 2, 9),
       ("Test result 6", "Test result description 6", "2016-10-21 06:00:00", 7, 8),
       ("Test result 7", "Test result description 7", "2016-10-21 07:00:00", 8, 5),
       ("Test result 8", "Test result description 8", "2016-10-21 08:00:00", 1, 1),
       ("Test result 9", "Test result description 9", "2016-10-21 09:00:00", 5, 2),
       ("Test result 10", "Test result description 10", "2016-10-21 10:00:00", 6, 3);

-- * TREATMENT (TID*, Text, Name, Type, PrefOrder, UID, OID)
--  * TREATMENT.OID -> ONLINE_CLINICIAN.OID
--  * TREATMENT.UID -> HOME_USER.UID
INSERT INTO TREATMENT (Text, Name, Type, PrefOrder, UID, OID)
VALUES ("Treatment text 1", "Treatment name 1", "Treatment type 1", "Pref-Order 1", 3, 8),
       ("Treatment text 2", "Treatment name 2", "Treatment type 2", "Pref-Order 2", 4, 4),
       ("Treatment text 3", "Treatment name 3", "Treatment type 3", "Pref-Order 3", 6, 6),
       ("Treatment text 4", "Treatment name 4", "Treatment type 4", "Pref-Order 4", 7, 4),
       ("Treatment text 5", "Treatment name 5", "Treatment type 5", "Pref-Order 5", 9, 6),
       ("Treatment text 6", "Treatment name 6", "Treatment type 6", "Pref-Order 6", 6, 5),
       ("Treatment text 7", "Treatment name 7", "Treatment type 7", "Pref-Order 7", 5, 9),
       ("Treatment text 8", "Treatment name 8", "Treatment type 8", "Pref-Order 8", 3, 8),
       ("Treatment text 9", "Treatment name 9", "Treatment type 9", "Pref-Order 9", 7, 7),
       ("Treatment text 10", "Treatment name 10", "Treatment type 10", "Pref-Order 10", 1, 4);

-- * USER_SIGNS_SYM (UID*, Time*, Severity, Description)
--  * USER_SIGNS_SYM.UID -> HOME_USER.UID
INSERT INTO USER_SIGNS_SYM (UID, Time, Severity, Description)
VALUES (6, "2016-10-21 01:00:00", "Severity 1", "Description 1"),
       (4, "2016-10-21 02:00:00", "Severity 2", "Description 2"),
       (2, "2016-10-21 03:00:00", "Severity 3", "Description 3"),
       (4, "2016-10-21 04:00:00", "Severity 4", "Description 4"),
       (9, "2016-10-21 05:00:00", "Severity 5", "Description 5"),
       (8, "2016-10-21 06:00:00", "Severity 6", "Description 6"),
       (9, "2016-10-21 07:00:00", "Severity 7", "Description 7"),
       (6, "2016-10-21 08:00:00", "Severity 8", "Description 8"),
       (7, "2016-10-21 09:00:00", "Severity 9", "Description 9"),
       (6, "2016-10-21 10:00:00", "Severity 10", "Description 10");

-- # Level 3

-- * ANSWER (AID*, AText, Type, ATimestamp, QID, OID)
--  * ANSWER.QID -> QUESTION.QID
--  * ANSWER.OID -> ONLINE_CLINICIAN.OID
INSERT INTO ANSWER (AText, Type, ATimestamp, QID, OID)
VALUES ("Anser text 1", "private", "2016-10-21 01:00:00", 4, 6),
       ("Anser text 2", "private", "2016-10-21 02:00:00", 2, 2),
       ("Anser text 3", "private", "2016-10-21 03:00:00", 3, 3),
       ("Anser text 4", "private", "2016-10-21 04:00:00", 9, 9),
       ("Anser text 5", "private", "2016-10-21 05:00:00", 4, 6),
       ("Anser text 6", "private", "2016-10-21 06:00:00", 2, 4),
       ("Anser text 7", "private", "2016-10-21 07:00:00", 1, 3),
       ("Anser text 8", "private", "2016-10-21 08:00:00", 3, 7),
       ("Anser text 9", "private", "2016-10-21 09:00:00", 5, 3),
       ("Anser text 10", "private", "2016-10-21 10:00:00", 6, 4);
UPDATE ANSWER
SET Type = (SELECT QUESTION.Type
            FROM QUESTION
            WHERE QUESTION.QID = ANSWER.QID);

-- * DIAG_DISEASE (UID*, OID*, DTimestamp*, ICD10CODE*)
--  * DIAG_DISEASE.(UID, OID, DTimestamp) -> DIAGNOSIS.(UID, OID, DTimestamp)
--  * DIAG_DISEASE.ICD10CODE -> DISEASE.ICD10CODE
INSERT INTO DIAG_DISEASE (UID, OID, DTimestamp, ICD10CODE)
VALUES (1, 1, "2016-10-21 01:00:00", "L20"),
       (2, 2, "2016-10-21 02:00:00", "L21"),
       (3, 3, "2016-10-21 03:00:00", "L22"),
       (2, 1, "2016-10-21 04:00:00", "L23"),
       (5, 5, "2016-10-21 05:00:00", "L24"),
       (6, 7, "2016-10-21 06:00:00", "L25"),
       (1, 8, "2016-10-21 08:00:00", "L26"),
       (8, 7, "2016-10-21 07:00:00", "L27"),
       (9, 3, "2016-10-21 09:00:00", "L28"),
       (4, 2, "2016-10-21 10:00:00", "L29");

-- Output

-- [45, 20:52:40] INSERT INTO DISEASE (ICD10CODE, Description)
-- VALUES ("L20", "Atopic dermatitis"),
--        ("L21", "Seborrhoeic dermatitis"),
--        ("L22", "Diaper [napkin] dermatitis"),
--        ("L23", "Allergic contact dermatitis"),
--        ("L24", "Irritant contact dermatitis"),
--        ("L25", "Unspecified contact dermatitis"),
--        ("L26", "Exfoliative dermatitis"),
--        ("L27", "Dermatitis due to substances taken internally"),
--        ("L28", "Lichen simplex chronicus and prurigo"),
--        ("L29", "Pruritus"): Running...
-- [45, 20:52:40] INSERT INTO DISEASE (ICD10CODE, Description)
-- VALUES ("L20", "Atopic dermatitis"),
--        ("L21", "Seborrhoeic dermatitis"),
--        ("L22", "Diaper [napkin] dermatitis"),
--        ("L23", "Allergic contact dermatitis"),
--        ("L24", "Irritant contact dermatitis"),
--        ("L25", "Unspecified contact dermatitis"),
--        ("L26", "Exfoliative dermatitis"),
--        ("L27", "Dermatitis due to substances taken internally"),
--        ("L28", "Lichen simplex chronicus and prurigo"),
--        ("L29", "Pruritus"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [46, 20:52:40] INSERT INTO HCHT (`Condition`, Process, Toolname, Method)
-- VALUES ("Condition 1", "Process 1", "Toolname 1", "Method 1"),
--        ("Condition 2", "Process 2", "Toolname 2", "Method 2"),
--        ("Condition 3", "Process 3", "Toolname 3", "Method 3"),
--        ("Condition 4", "Process 4", "Toolname 4", "Method 4"),
--        ("Condition 5", "Process 5", "Toolname 5", "Method 5"),
--        ("Condition 6", "Process 6", "Toolname 6", "Method 6"),
--        ("Condition 7", "Process 7", "Toolname 7", "Method 7"),
--        ("Condition 8", "Process 8", "Toolname 8", "Method 8"),
--        ("Condition 9", "Process 9", "Toolname 9", "Method 9"),
--        ("Condition 10", "Process 10", "Toolname 10", "Method 10"): Running...
-- [46, 20:52:40] INSERT INTO HCHT (`Condition`, Process, Toolname, Method)
-- VALUES ("Condition 1", "Process 1", "Toolname 1", "Method 1"),
--        ("Condition 2", "Process 2", "Toolname 2", "Method 2"),
--        ("Condition 3", "Process 3", "Toolname 3", "Method 3"),
--        ("Condition 4", "Process 4", "Toolname 4", "Method 4"),
--        ("Condition 5", "Process 5", "Toolname 5", "Method 5"),
--        ("Condition 6", "Process 6", "Toolname 6", "Method 6"),
--        ("Condition 7", "Process 7", "Toolname 7", "Method 7"),
--        ("Condition 8", "Process 8", "Toolname 8", "Method 8"),
--        ("Condition 9", "Process 9", "Toolname 9", "Method 9"),
--        ("Condition 10", "Process 10", "Toolname 10", "Method 10"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [47, 20:52:40] INSERT INTO ONLINE_CLINICIAN (Phone, eMail, FName, LName, Address)
-- VALUES ("1234567891", "1@online.clinician", "Dr 1", "MD 1", "Place 1"),
--        ("1234567892", "2@online.clinician", "Dr 2", "MD 2", "Place 2"),
--        ("1234567893", "3@online.clinician", "Dr 3", "MD 3", "Place 3"),
--        ("1234567894", "4@online.clinician", "Dr 4", "MD 4", "Place 4"),
--        ("1234567895", "5@online.clinician", "Dr 5", "MD 5", "Place 5"),
--        ("1234567896", "6@online.clinician", "Dr 6", "MD 6", "Place 6"),
--        ("1234567897", "7@online.clinician", "Dr 7", "MD 7", "Place 7"),
--        ("1234567898", "8@online.clinician", "Dr 8", "MD 8", "Place 8"),
--        ("1234567899", "9@online.clinician", "Dr 9", "MD 9", "Place 9"),
--        ("1234567900", "10@online.clinician", "Dr 10", "MD 10", "Place 10"): Running...
-- [47, 20:52:40] INSERT INTO ONLINE_CLINICIAN (Phone, eMail, FName, LName, Address)
-- VALUES ("1234567891", "1@online.clinician", "Dr 1", "MD 1", "Place 1"),
--        ("1234567892", "2@online.clinician", "Dr 2", "MD 2", "Place 2"),
--        ("1234567893", "3@online.clinician", "Dr 3", "MD 3", "Place 3"),
--        ("1234567894", "4@online.clinician", "Dr 4", "MD 4", "Place 4"),
--        ("1234567895", "5@online.clinician", "Dr 5", "MD 5", "Place 5"),
--        ("1234567896", "6@online.clinician", "Dr 6", "MD 6", "Place 6"),
--        ("1234567897", "7@online.clinician", "Dr 7", "MD 7", "Place 7"),
--        ("1234567898", "8@online.clinician", "Dr 8", "MD 8", "Place 8"),
--        ("1234567899", "9@online.clinician", "Dr 9", "MD 9", "Place 9"),
--        ("1234567900", "10@online.clinician", "Dr 10", "MD 10", "Place 10"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [48, 20:52:40] INSERT INTO HOME_USER (Phone, eMail, FName, LName, Address, OID)
-- VALUES ("0987654321", "1@home.user", "Real 1", "Person 1", "Home 1", 1),
--        ("0987654322", "2@home.user", "Real 2", "Person 2", "Home 2", 7),
--        ("0987654323", "3@home.user", "Real 3", "Person 3", "Home 3", 3),
--        ("0987654324", "4@home.user", "Real 4", "Person 4", "Home 4", 5),
--        ("0987654325", "5@home.user", "Real 5", "Person 5", "Home 5", 5),
--        ("0987654326", "6@home.user", "Real 6", "Person 6", "Home 6", 3),
--        ("0987654327", "7@home.user", "Real 7", "Person 7", "Home 7", 7),
--        ("0987654328", "8@home.user", "Real 8", "Person 8", "Home 8", 10),
--        ("0987654329", "9@home.user", "Real 9", "Person 9", "Home 9", 9),
--        ("0987654330", "10@home.user", "Real 10", "Person 10", "Home 10", 10): Running...
-- [48, 20:52:40] INSERT INTO HOME_USER (Phone, eMail, FName, LName, Address, OID)
-- VALUES ("0987654321", "1@home.user", "Real 1", "Person 1", "Home 1", 1),
--        ("0987654322", "2@home.user", "Real 2", "Person 2", "Home 2", 7),
--        ("0987654323", "3@home.user", "Real 3", "Person 3", "Home 3", 3),
--        ("0987654324", "4@home.user", "Real 4", "Person 4", "Home 4", 5),
--        ("0987654325", "5@home.user", "Real 5", "Person 5", "Home 5", 5),
--        ("0987654326", "6@home.user", "Real 6", "Person 6", "Home 6", 3),
--        ("0987654327", "7@home.user", "Real 7", "Person 7", "Home 7", 7),
--        ("0987654328", "8@home.user", "Real 8", "Person 8", "Home 8", 10),
--        ("0987654329", "9@home.user", "Real 9", "Person 9", "Home 9", 9),
--        ("0987654330", "10@home.user", "Real 10", "Person 10", "Home 10", 10): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [49, 20:52:40] INSERT INTO CONSULTATION (UID, OID, Time)
-- VALUES (1, 1, "2016-10-21 01:00:00"),
--        (2, 2, "2016-10-21 02:00:00"),
--        (3, 3, "2016-10-21 03:00:00"),
--        (2, 1, "2016-10-21 04:00:00"),
--        (5, 5, "2016-10-21 05:00:00"),
--        (6, 7, "2016-10-21 06:00:00"),
--        (8, 7, "2016-10-21 07:00:00"),
--        (1, 8, "2016-10-21 08:00:00"),
--        (9, 3, "2016-10-21 09:00:00"),
--        (4, 2, "2016-10-21 10:00:00"): Running...
-- [49, 20:52:40] INSERT INTO CONSULTATION (UID, OID, Time)
-- VALUES (1, 1, "2016-10-21 01:00:00"),
--        (2, 2, "2016-10-21 02:00:00"),
--        (3, 3, "2016-10-21 03:00:00"),
--        (2, 1, "2016-10-21 04:00:00"),
--        (5, 5, "2016-10-21 05:00:00"),
--        (6, 7, "2016-10-21 06:00:00"),
--        (8, 7, "2016-10-21 07:00:00"),
--        (1, 8, "2016-10-21 08:00:00"),
--        (9, 3, "2016-10-21 09:00:00"),
--        (4, 2, "2016-10-21 10:00:00"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [50, 20:52:40] INSERT INTO DIAGNOSIS (UID, OID, DTimestamp, DText)
-- VALUES (1, 1, "2016-10-21 01:00:00", "Diagnosis 1"),
--        (2, 2, "2016-10-21 02:00:00", "Diagnosis 2"),
--        (3, 3, "2016-10-21 03:00:00", "Diagnosis 3"),
--        (2, 1, "2016-10-21 04:00:00", "Diagnosis 4"),
--        (5, 5, "2016-10-21 05:00:00", "Diagnosis 5"),
--        (6, 7, "2016-10-21 06:00:00", "Diagnosis 6"),
--        (8, 7, "2016-10-21 07:00:00", "Diagnosis 7"),
--        (1, 8, "2016-10-21 08:00:00", "Diagnosis 8"),
--        (9, 3, "2016-10-21 09:00:00", "Diagnosis 9"),
--        (4, 2, "2016-10-21 10:00:00", "Diagnosis 10"): Running...
-- [50, 20:52:40] INSERT INTO DIAGNOSIS (UID, OID, DTimestamp, DText)
-- VALUES (1, 1, "2016-10-21 01:00:00", "Diagnosis 1"),
--        (2, 2, "2016-10-21 02:00:00", "Diagnosis 2"),
--        (3, 3, "2016-10-21 03:00:00", "Diagnosis 3"),
--        (2, 1, "2016-10-21 04:00:00", "Diagnosis 4"),
--        (5, 5, "2016-10-21 05:00:00", "Diagnosis 5"),
--        (6, 7, "2016-10-21 06:00:00", "Diagnosis 6"),
--        (8, 7, "2016-10-21 07:00:00", "Diagnosis 7"),
--        (1, 8, "2016-10-21 08:00:00", "Diagnosis 8"),
--        (9, 3, "2016-10-21 09:00:00", "Diagnosis 9"),
--        (4, 2, "2016-10-21 10:00:00", "Diagnosis 10"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [51, 20:52:40] INSERT INTO QUESTION (QText, Type, QTimestamp, UID)
-- VALUES ("Question 1?", "private", "2016-10-21 01:00:00", 1),
--        ("Question 2?", "open", "2016-10-21 02:00:00", 2),
--        ("Question 3?", "open", "2016-10-21 03:00:00", 5),
--        ("Question 4?", "open", "2016-10-21 04:00:00", 9),
--        ("Question 5?", "private", "2016-10-21 05:00:00", 1),
--        ("Question 6?", "private", "2016-10-21 06:00:00", 2),
--        ("Question 7?", "open", "2016-10-21 07:00:00", 2),
--        ("Question 8?", "private", "2016-10-21 08:00:00", 3),
--        ("Question 9?", "private", "2016-10-21 09:00:00", 4),
--        ("Question 10?", "private", "2016-10-21 10:00:00", 5): Running...
-- [51, 20:52:40] INSERT INTO QUESTION (QText, Type, QTimestamp, UID)
-- VALUES ("Question 1?", "private", "2016-10-21 01:00:00", 1),
--        ("Question 2?", "open", "2016-10-21 02:00:00", 2),
--        ("Question 3?", "open", "2016-10-21 03:00:00", 5),
--        ("Question 4?", "open", "2016-10-21 04:00:00", 9),
--        ("Question 5?", "private", "2016-10-21 05:00:00", 1),
--        ("Question 6?", "private", "2016-10-21 06:00:00", 2),
--        ("Question 7?", "open", "2016-10-21 07:00:00", 2),
--        ("Question 8?", "private", "2016-10-21 08:00:00", 3),
--        ("Question 9?", "private", "2016-10-21 09:00:00", 4),
--        ("Question 10?", "private", "2016-10-21 10:00:00", 5): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [52, 20:52:40] INSERT INTO TEST_RECOMMENDATION (UID, OID, Time, Description, Type, TESTID)
-- VALUES (1, 7, "2016-10-21 01:00:00", "Test recommendation 1", "Type 1", 5),
--        (3, 8, "2016-10-21 02:00:00", "Test recommendation 2", "Type 2", 2),
--        (7, 6, "2016-10-21 03:00:00", "Test recommendation 3", "Type 3", 4),
--        (4, 1, "2016-10-21 04:00:00", "Test recommendation 4", "Type 4", 6),
--        (4, 3, "2016-10-21 05:00:00", "Test recommendation 5", "Type 5", 3),
--        (2, 4, "2016-10-21 06:00:00", "Test recommendation 6", "Type 6", 5),
--        (8, 8, "2016-10-21 07:00:00", "Test recommendation 7", "Type 7", 7),
--        (8, 1, "2016-10-21 08:00:00", "Test recommendation 8", "Type 8", 9),
--        (9, 3, "2016-10-21 09:00:00", "Test recommendation 9", "Type 9", 4),
--        (10, 4, "2016-10-21 10:00:00", "Test recommendation 10", "Type 10", 5): Running...
-- [52, 20:52:40] INSERT INTO TEST_RECOMMENDATION (UID, OID, Time, Description, Type, TESTID)
-- VALUES (1, 7, "2016-10-21 01:00:00", "Test recommendation 1", "Type 1", 5),
--        (3, 8, "2016-10-21 02:00:00", "Test recommendation 2", "Type 2", 2),
--        (7, 6, "2016-10-21 03:00:00", "Test recommendation 3", "Type 3", 4),
--        (4, 1, "2016-10-21 04:00:00", "Test recommendation 4", "Type 4", 6),
--        (4, 3, "2016-10-21 05:00:00", "Test recommendation 5", "Type 5", 3),
--        (2, 4, "2016-10-21 06:00:00", "Test recommendation 6", "Type 6", 5),
--        (8, 8, "2016-10-21 07:00:00", "Test recommendation 7", "Type 7", 7),
--        (8, 1, "2016-10-21 08:00:00", "Test recommendation 8", "Type 8", 9),
--        (9, 3, "2016-10-21 09:00:00", "Test recommendation 9", "Type 9", 4),
--        (10, 4, "2016-10-21 10:00:00", "Test recommendation 10", "Type 10", 5): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [53, 20:52:40] INSERT INTO TEST_RESULT (Result, Description, Time, UID, TESTID)
-- VALUES ("Test result 1", "Test result description 1", "2016-10-21 01:00:00", 6, 4),
--        ("Test result 2", "Test result description 2", "2016-10-21 02:00:00", 3, 6),
--        ("Test result 3", "Test result description 3", "2016-10-21 03:00:00", 4, 3),
--        ("Test result 4", "Test result description 4", "2016-10-21 04:00:00", 9, 4),
--        ("Test result 5", "Test result description 5", "2016-10-21 05:00:00", 2, 9),
--        ("Test result 6", "Test result description 6", "2016-10-21 06:00:00", 7, 8),
--        ("Test result 7", "Test result description 7", "2016-10-21 07:00:00", 8, 5),
--        ("Test result 8", "Test result description 8", "2016-10-21 08:00:00", 1, 1),
--        ("Test result 9", "Test result description 9", "2016-10-21 09:00:00", 5, 2),
--        ("Test result 10", "Test result description 10", "2016-10-21 10:00:00", 6, 3): Running...
-- [53, 20:52:40] INSERT INTO TEST_RESULT (Result, Description, Time, UID, TESTID)
-- VALUES ("Test result 1", "Test result description 1", "2016-10-21 01:00:00", 6, 4),
--        ("Test result 2", "Test result description 2", "2016-10-21 02:00:00", 3, 6),
--        ("Test result 3", "Test result description 3", "2016-10-21 03:00:00", 4, 3),
--        ("Test result 4", "Test result description 4", "2016-10-21 04:00:00", 9, 4),
--        ("Test result 5", "Test result description 5", "2016-10-21 05:00:00", 2, 9),
--        ("Test result 6", "Test result description 6", "2016-10-21 06:00:00", 7, 8),
--        ("Test result 7", "Test result description 7", "2016-10-21 07:00:00", 8, 5),
--        ("Test result 8", "Test result description 8", "2016-10-21 08:00:00", 1, 1),
--        ("Test result 9", "Test result description 9", "2016-10-21 09:00:00", 5, 2),
--        ("Test result 10", "Test result description 10", "2016-10-21 10:00:00", 6, 3): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [54, 20:52:40] INSERT INTO TREATMENT (Text, Name, Type, PrefOrder, UID, OID)
-- VALUES ("Treatment text 1", "Treatment name 1", "Treatment type 1", "Pref-Order 1", 3, 8),
--        ("Treatment text 2", "Treatment name 2", "Treatment type 2", "Pref-Order 2", 4, 4),
--        ("Treatment text 3", "Treatment name 3", "Treatment type 3", "Pref-Order 3", 6, 6),
--        ("Treatment text 4", "Treatment name 4", "Treatment type 4", "Pref-Order 4", 7, 4),
--        ("Treatment text 5", "Treatment name 5", "Treatment type 5", "Pref-Order 5", 9, 6),
--        ("Treatment text 6", "Treatment name 6", "Treatment type 6", "Pref-Order 6", 6, 5),
--        ("Treatment text 7", "Treatment name 7", "Treatment type 7", "Pref-Order 7", 5, 9),
--        ("Treatment text 8", "Treatment name 8", "Treatment type 8", "Pref-Order 8", 3, 8),
--        ("Treatment text 9", "Treatment name 9", "Treatment type 9", "Pref-Order 9", 7, 7),
--        ("Treatment text 10", "Treatment name 10", "Treatment type 10", "Pref-Order 10", 1, 4): Running...
-- [54, 20:52:40] INSERT INTO TREATMENT (Text, Name, Type, PrefOrder, UID, OID)
-- VALUES ("Treatment text 1", "Treatment name 1", "Treatment type 1", "Pref-Order 1", 3, 8),
--        ("Treatment text 2", "Treatment name 2", "Treatment type 2", "Pref-Order 2", 4, 4),
--        ("Treatment text 3", "Treatment name 3", "Treatment type 3", "Pref-Order 3", 6, 6),
--        ("Treatment text 4", "Treatment name 4", "Treatment type 4", "Pref-Order 4", 7, 4),
--        ("Treatment text 5", "Treatment name 5", "Treatment type 5", "Pref-Order 5", 9, 6),
--        ("Treatment text 6", "Treatment name 6", "Treatment type 6", "Pref-Order 6", 6, 5),
--        ("Treatment text 7", "Treatment name 7", "Treatment type 7", "Pref-Order 7", 5, 9),
--        ("Treatment text 8", "Treatment name 8", "Treatment type 8", "Pref-Order 8", 3, 8),
--        ("Treatment text 9", "Treatment name 9", "Treatment type 9", "Pref-Order 9", 7, 7),
--        ("Treatment text 10", "Treatment name 10", "Treatment type 10", "Pref-Order 10", 1, 4): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [55, 20:52:40] INSERT INTO USER_SIGNS_SYM (UID, Time, Severity, Description)
-- VALUES (6, "2016-10-21 01:00:00", "Severity 1", "Description 1"),
--        (4, "2016-10-21 02:00:00", "Severity 2", "Description 2"),
--        (2, "2016-10-21 03:00:00", "Severity 3", "Description 3"),
--        (4, "2016-10-21 04:00:00", "Severity 4", "Description 4"),
--        (9, "2016-10-21 05:00:00", "Severity 5", "Description 5"),
--        (8, "2016-10-21 06:00:00", "Severity 6", "Description 6"),
--        (9, "2016-10-21 07:00:00", "Severity 7", "Description 7"),
--        (6, "2016-10-21 08:00:00", "Severity 8", "Description 8"),
--        (7, "2016-10-21 09:00:00", "Severity 9", "Description 9"),
--        (6, "2016-10-21 10:00:00", "Severity 10", "Description 10"): Running...
-- [55, 20:52:40] INSERT INTO USER_SIGNS_SYM (UID, Time, Severity, Description)
-- VALUES (6, "2016-10-21 01:00:00", "Severity 1", "Description 1"),
--        (4, "2016-10-21 02:00:00", "Severity 2", "Description 2"),
--        (2, "2016-10-21 03:00:00", "Severity 3", "Description 3"),
--        (4, "2016-10-21 04:00:00", "Severity 4", "Description 4"),
--        (9, "2016-10-21 05:00:00", "Severity 5", "Description 5"),
--        (8, "2016-10-21 06:00:00", "Severity 6", "Description 6"),
--        (9, "2016-10-21 07:00:00", "Severity 7", "Description 7"),
--        (6, "2016-10-21 08:00:00", "Severity 8", "Description 8"),
--        (7, "2016-10-21 09:00:00", "Severity 9", "Description 9"),
--        (6, "2016-10-21 10:00:00", "Severity 10", "Description 10"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [56, 20:52:40] INSERT INTO ANSWER (AText, Type, ATimestamp, QID, OID)
-- VALUES ("Anser text 1", "private", "2016-10-21 01:00:00", 4, 6),
--        ("Anser text 2", "private", "2016-10-21 02:00:00", 2, 2),
--        ("Anser text 3", "private", "2016-10-21 03:00:00", 3, 3),
--        ("Anser text 4", "private", "2016-10-21 04:00:00", 9, 9),
--        ("Anser text 5", "private", "2016-10-21 05:00:00", 4, 6),
--        ("Anser text 6", "private", "2016-10-21 06:00:00", 2, 4),
--        ("Anser text 7", "private", "2016-10-21 07:00:00", 1, 3),
--        ("Anser text 8", "private", "2016-10-21 08:00:00", 3, 7),
--        ("Anser text 9", "private", "2016-10-21 09:00:00", 5, 3),
--        ("Anser text 10", "private", "2016-10-21 10:00:00", 6, 4): Running...
-- [56, 20:52:40] INSERT INTO ANSWER (AText, Type, ATimestamp, QID, OID)
-- VALUES ("Anser text 1", "private", "2016-10-21 01:00:00", 4, 6),
--        ("Anser text 2", "private", "2016-10-21 02:00:00", 2, 2),
--        ("Anser text 3", "private", "2016-10-21 03:00:00", 3, 3),
--        ("Anser text 4", "private", "2016-10-21 04:00:00", 9, 9),
--        ("Anser text 5", "private", "2016-10-21 05:00:00", 4, 6),
--        ("Anser text 6", "private", "2016-10-21 06:00:00", 2, 4),
--        ("Anser text 7", "private", "2016-10-21 07:00:00", 1, 3),
--        ("Anser text 8", "private", "2016-10-21 08:00:00", 3, 7),
--        ("Anser text 9", "private", "2016-10-21 09:00:00", 5, 3),
--        ("Anser text 10", "private", "2016-10-21 10:00:00", 6, 4): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [57, 20:52:40] UPDATE ANSWER
-- SET Type = (SELECT QUESTION.Type
--             FROM QUESTION
--             WHERE QUESTION.QID = ANSWER.QID): Running...
-- [57, 20:52:40] UPDATE ANSWER
-- SET Type = (SELECT QUESTION.Type
--             FROM QUESTION
--             WHERE QUESTION.QID = ANSWER.QID): 6 row(s) affected
-- Rows matched: 10  Changed: 6  Warnings: 0
-- [58, 20:52:40] INSERT INTO DIAG_DISEASE (UID, OID, DTimestamp, ICD10CODE)
-- VALUES (1, 1, "2016-10-21 01:00:00", "L20"),
--        (2, 2, "2016-10-21 02:00:00", "L21"),
--        (3, 3, "2016-10-21 03:00:00", "L22"),
--        (2, 1, "2016-10-21 04:00:00", "L23"),
--        (5, 5, "2016-10-21 05:00:00", "L24"),
--        (6, 7, "2016-10-21 06:00:00", "L25"),
--        (1, 8, "2016-10-21 08:00:00", "L26"),
--        (8, 7, "2016-10-21 07:00:00", "L27"),
--        (9, 3, "2016-10-21 09:00:00", "L28"),
--        (4, 2, "2016-10-21 10:00:00", "L29"): Running...
-- [58, 20:52:40] INSERT INTO DIAG_DISEASE (UID, OID, DTimestamp, ICD10CODE)
-- VALUES (1, 1, "2016-10-21 01:00:00", "L20"),
--        (2, 2, "2016-10-21 02:00:00", "L21"),
--        (3, 3, "2016-10-21 03:00:00", "L22"),
--        (2, 1, "2016-10-21 04:00:00", "L23"),
--        (5, 5, "2016-10-21 05:00:00", "L24"),
--        (6, 7, "2016-10-21 06:00:00", "L25"),
--        (1, 8, "2016-10-21 08:00:00", "L26"),
--        (8, 7, "2016-10-21 07:00:00", "L27"),
--        (9, 3, "2016-10-21 09:00:00", "L28"),
--        (4, 2, "2016-10-21 10:00:00", "L29"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
