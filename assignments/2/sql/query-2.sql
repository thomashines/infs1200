

-- Query 2

-- List healthcare consultation requests (symptoms, questions, diagnostic test
-- results) for home users.

SELECT CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type, CONSULTATION_REQUESTS.Description
FROM (
      SELECT UID, "Question" AS Type, QText AS Description
      FROM QUESTION
      UNION
      SELECT UID, "Symptom" AS Type, Description AS Description
      FROM USER_SIGNS_SYM
      UNION
      SELECT UID, "Test result" AS Type, Description AS Description
      FROM TEST_RESULT
     ) AS CONSULTATION_REQUESTS
ORDER BY CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type;

-- Output

-- [77, 21:02:17] SELECT CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type, CONSULTATION_REQUESTS.Description
-- FROM (
--       SELECT UID, "Question" AS Type, QText AS Description
--       FROM QUESTION
--       UNION
--       SELECT UID, "Symptom" AS Type, Description AS Description
--       FROM USER_SIGNS_SYM
--       UNION
--       SELECT UID, "Test result" AS Type, Description AS Description
--       FROM TEST_RESULT
--      ) AS CONSULTATION_REQUESTS
-- ORDER BY CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type
-- LIMIT 0, 1000
-- : Running...
-- [77, 21:02:17] SELECT CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type, CONSULTATION_REQUESTS.Description
-- FROM (
--       SELECT UID, "Question" AS Type, QText AS Description
--       FROM QUESTION
--       UNION
--       SELECT UID, "Symptom" AS Type, Description AS Description
--       FROM USER_SIGNS_SYM
--       UNION
--       SELECT UID, "Test result" AS Type, Description AS Description
--       FROM TEST_RESULT
--      ) AS CONSULTATION_REQUESTS
-- ORDER BY CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type
-- LIMIT 0, 1000
-- : Fetching...
-- [77, 21:02:17] SELECT CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type, CONSULTATION_REQUESTS.Description
-- FROM (
--       SELECT UID, "Question" AS Type, QText AS Description
--       FROM QUESTION
--       UNION
--       SELECT UID, "Symptom" AS Type, Description AS Description
--       FROM USER_SIGNS_SYM
--       UNION
--       SELECT UID, "Test result" AS Type, Description AS Description
--       FROM TEST_RESULT
--      ) AS CONSULTATION_REQUESTS
-- ORDER BY CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type
-- LIMIT 0, 1000
-- : 30 row(s) returned
-- # UID, Type, Description
-- '1', 'Question', 'Question 1?'
-- '1', 'Question', 'Question 5?'
-- '1', 'Test result', 'Test result description 8'
-- '2', 'Question', 'Question 2?'
-- '2', 'Question', 'Question 6?'
-- '2', 'Question', 'Question 7?'
-- '2', 'Symptom', 'Description 3'
-- '2', 'Test result', 'Test result description 5'
-- '3', 'Question', 'Question 8?'
-- '3', 'Test result', 'Test result description 2'
-- '4', 'Question', 'Question 9?'
-- '4', 'Symptom', 'Description 2'
-- '4', 'Symptom', 'Description 4'
-- '4', 'Test result', 'Test result description 3'
-- '5', 'Question', 'Question 3?'
-- '5', 'Question', 'Question 10?'
-- '5', 'Test result', 'Test result description 9'
-- '6', 'Symptom', 'Description 1'
-- '6', 'Symptom', 'Description 8'
-- '6', 'Symptom', 'Description 10'
-- '6', 'Test result', 'Test result description 1'
-- '6', 'Test result', 'Test result description 10'
-- '7', 'Symptom', 'Description 9'
-- '7', 'Test result', 'Test result description 6'
-- '8', 'Symptom', 'Description 6'
-- '8', 'Test result', 'Test result description 7'
-- '9', 'Question', 'Question 4?'
-- '9', 'Symptom', 'Description 5'
-- '9', 'Symptom', 'Description 7'
-- '9', 'Test result', 'Test result description 4'
