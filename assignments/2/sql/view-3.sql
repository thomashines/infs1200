

-- View 3

-- Consultation View:
-- Generate a view that lists all the questions and answers asked by home users
-- and answered by online clinicians. The view should firstly sort on the dates
-- of questions asked, then sort on the questions.

DROP VIEW IF EXISTS Consultation_View;
CREATE VIEW Consultation_View AS
SELECT DATE(QTimestamp) AS Date, QText, Atext
FROM QUESTION, ANSWER
WHERE ANSWER.QID = QUESTION.QID
ORDER BY Date DESC, QText;

SELECT *
FROM Consultation_View;

-- Output

-- [70, 20:57:15] DROP VIEW IF EXISTS Consultation_View: Running...
-- [70, 20:57:15] DROP VIEW IF EXISTS Consultation_View: 0 row(s) affected, 1 warning(s):
-- 1051 Unknown table 'test.Consultation_View'
-- [71, 20:57:15] CREATE VIEW Consultation_View AS
-- SELECT DATE(QTimestamp) AS Date, QText, Atext
-- FROM QUESTION, ANSWER
-- WHERE ANSWER.QID = QUESTION.QID
-- ORDER BY Date DESC, QText: Running...
-- [71, 20:57:15] CREATE VIEW Consultation_View AS
-- SELECT DATE(QTimestamp) AS Date, QText, Atext
-- FROM QUESTION, ANSWER
-- WHERE ANSWER.QID = QUESTION.QID
-- ORDER BY Date DESC, QText: 0 row(s) affected
-- [72, 20:57:15] SELECT *
-- FROM Consultation_View
-- LIMIT 0, 1000
-- : Running...
-- [72, 20:57:15] SELECT *
-- FROM Consultation_View
-- LIMIT 0, 1000
-- : Fetching...
-- [72, 20:57:15] SELECT *
-- FROM Consultation_View
-- LIMIT 0, 1000
-- : 10 row(s) returned
-- # Date, QText, Atext
-- '2016-10-21', 'Question 1?', 'Anser text 7'
-- '2016-10-21', 'Question 2?', 'Anser text 2'
-- '2016-10-21', 'Question 2?', 'Anser text 6'
-- '2016-10-21', 'Question 3?', 'Anser text 3'
-- '2016-10-21', 'Question 3?', 'Anser text 8'
-- '2016-10-21', 'Question 4?', 'Anser text 1'
-- '2016-10-21', 'Question 4?', 'Anser text 5'
-- '2016-10-21', 'Question 5?', 'Anser text 9'
-- '2016-10-21', 'Question 6?', 'Anser text 10'
-- '2016-10-21', 'Question 9?', 'Anser text 4'
