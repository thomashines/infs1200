

-- View 2

-- Clinician Summary View:
-- For every online clinician, generate a list of his/her home users. Show all
-- details of home users who relate to the clinician. Provide also a count of
-- home users per clinician as summary.

-- Variables:
-- Adjust the online clinician this view is for by changing OID on line 27.

-- View for home user counts
DROP VIEW IF EXISTS Clinician_Home_User_Counts;
CREATE VIEW Clinician_Home_User_Counts AS
SELECT OID, COUNT(*) AS HomeUserCount
FROM HOME_USER
GROUP BY OID;

-- View for all online clinicians
DROP VIEW IF EXISTS Clinician_Summary_View;
CREATE VIEW Clinician_Summary_View AS
SELECT HOME_USER.OID, HomeUserCount, Phone, eMail, FName, LName, Address
FROM HOME_USER, Clinician_Home_User_Counts
WHERE Clinician_Home_User_Counts.OID = HOME_USER.OID
ORDER BY HOME_USER.OID;

-- Filter view for just one online clinician
SELECT *
FROM Clinician_Summary_View
WHERE OID = 3;

-- Output

-- [65, 20:56:21] DROP VIEW IF EXISTS Clinician_Home_User_Counts: Running...
-- [65, 20:56:21] DROP VIEW IF EXISTS Clinician_Home_User_Counts: 0 row(s) affected, 1 warning(s):
-- 1051 Unknown table 'test.Clinician_Home_User_Counts'
-- [66, 20:56:21] CREATE VIEW Clinician_Home_User_Counts AS
-- SELECT OID, COUNT(*) AS HomeUserCount
-- FROM HOME_USER
-- GROUP BY OID: Running...
-- [66, 20:56:21] CREATE VIEW Clinician_Home_User_Counts AS
-- SELECT OID, COUNT(*) AS HomeUserCount
-- FROM HOME_USER
-- GROUP BY OID: 0 row(s) affected
-- [67, 20:56:21] DROP VIEW IF EXISTS Clinician_Summary_View: Running...
-- [67, 20:56:21] DROP VIEW IF EXISTS Clinician_Summary_View: 0 row(s) affected, 1 warning(s):
-- 1051 Unknown table 'test.Clinician_Summary_View'
-- [68, 20:56:21] CREATE VIEW Clinician_Summary_View AS
-- SELECT HOME_USER.OID, HomeUserCount, Phone, eMail, FName, LName, Address
-- FROM HOME_USER, Clinician_Home_User_Counts
-- WHERE Clinician_Home_User_Counts.OID = HOME_USER.OID
-- ORDER BY HOME_USER.OID: Running...
-- [68, 20:56:21] CREATE VIEW Clinician_Summary_View AS
-- SELECT HOME_USER.OID, HomeUserCount, Phone, eMail, FName, LName, Address
-- FROM HOME_USER, Clinician_Home_User_Counts
-- WHERE Clinician_Home_User_Counts.OID = HOME_USER.OID
-- ORDER BY HOME_USER.OID: 0 row(s) affected
-- [69, 20:56:21] SELECT *
-- FROM Clinician_Summary_View
-- WHERE OID = 3
-- LIMIT 0, 1000
-- : Running...
-- [69, 20:56:21] SELECT *
-- FROM Clinician_Summary_View
-- WHERE OID = 3
-- LIMIT 0, 1000
-- : Fetching...
-- [69, 20:56:21] SELECT *
-- FROM Clinician_Summary_View
-- WHERE OID = 3
-- LIMIT 0, 1000
-- : 2 row(s) returned
-- # OID, HomeUserCount, Phone, eMail, FName, LName, Address
-- '3', '2', '0987654323', '3@home.user', 'Real 3', 'Person 3', 'Home 3'
-- '3', '2', '0987654326', '6@home.user', 'Real 6', 'Person 6', 'Home 6'
