

-- View 1

-- Home User View:
-- This view is to be generated for every home user. List the activities of
-- healthcare consultations. The listing should appear in the order of date,
-- and then by matters that the user consulted (symptoms, questions).

-- Assumptions:
-- "healthcare consultations" are questions, answers, symptoms, test results,
-- test recommendations and diagnoses

-- Variables:
-- Adjust the home user this view is for by changing UID on line 57.

-- View for activities
DROP VIEW IF EXISTS Home_User_View_Activities;
CREATE VIEW Home_User_View_Activities AS

-- Question by user
SELECT UID, DATE(QTimestamp) AS Date, "Question" AS Matter, QText AS Activity
FROM QUESTION

-- Answer by user
UNION
SELECT UID, DATE(ATimestamp) AS Date, "Answer" AS Matter, AText AS Activity
FROM ANSWER, QUESTION
WHERE QUESTION.QID = ANSWER.QID

-- Symptoms by user
UNION
SELECT UID, DATE(Time) AS Date, "Symptom" AS Matter, Description AS Activity
FROM USER_SIGNS_SYM

-- Test results by user
UNION
SELECT UID, DATE(Time) AS Date, "Test result" AS Matter, Description AS Activity
FROM TEST_RESULT

-- Test recommendations by user
UNION
SELECT UID, DATE(Time) AS Date, "Test recommendation" AS Matter, Description AS Activity
FROM TEST_RECOMMENDATION

-- Diagnoses by user
UNION
SELECT UID, DATE(DTimestamp) AS Date, "Diagnosis" AS Matter, DText AS Activity
FROM DIAGNOSIS;

-- View proper
DROP VIEW IF EXISTS Home_User_View;
CREATE VIEW Home_User_View AS
SELECT UID, Date, Matter, Activity
FROM Home_User_View_Activities
ORDER BY UID, Date DESC, Matter;

-- Query to get view for a specific home user
SELECT *
FROM Home_User_View
WHERE UID = 2;

-- Output

-- [60, 20:54:26] DROP VIEW IF EXISTS Home_User_View_Activities: Running...
-- [60, 20:54:26] DROP VIEW IF EXISTS Home_User_View_Activities: 0 row(s) affected, 1 warning(s):
-- 1051 Unknown table 'test.Home_User_View_Activities'
-- [61, 20:54:26] CREATE VIEW Home_User_View_Activities AS
--
-- -- Question by user
-- SELECT UID, DATE(QTimestamp) AS Date, "Question" AS Matter, QText AS Activity
-- FROM QUESTION
--
-- -- Answer by user
-- UNION
-- SELECT UID, DATE(ATimestamp) AS Date, "Answer" AS Matter, AText AS Activity
-- FROM ANSWER, QUESTION
-- WHERE QUESTION.QID = ANSWER.QID
--
-- -- Symptoms by user
-- UNION
-- SELECT UID, DATE(Time) AS Date, "Symptom" AS Matter, Description AS Activity
-- FROM USER_SIGNS_SYM
--
-- -- Test results by user
-- UNION
-- SELECT UID, DATE(Time) AS Date, "Test result" AS Matter, Description AS Activity
-- FROM TEST_RESULT
--
-- -- Test recommendations by user
-- UNION
-- SELECT UID, DATE(Time) AS Date, "Test recommendation" AS Matter, Description AS Activity
-- FROM TEST_RECOMMENDATION
--
-- -- Diagnoses by user
-- UNION
-- SELECT UID, DATE(DTimestamp) AS Date, "Diagnosis" AS Matter, DText AS Activity
-- FROM DIAGNOSIS: Running...
-- [61, 20:54:26] CREATE VIEW Home_User_View_Activities AS
--
-- -- Question by user
-- SELECT UID, DATE(QTimestamp) AS Date, "Question" AS Matter, QText AS Activity
-- FROM QUESTION
--
-- -- Answer by user
-- UNION
-- SELECT UID, DATE(ATimestamp) AS Date, "Answer" AS Matter, AText AS Activity
-- FROM ANSWER, QUESTION
-- WHERE QUESTION.QID = ANSWER.QID
--
-- -- Symptoms by user
-- UNION
-- SELECT UID, DATE(Time) AS Date, "Symptom" AS Matter, Description AS Activity
-- FROM USER_SIGNS_SYM
--
-- -- Test results by user
-- UNION
-- SELECT UID, DATE(Time) AS Date, "Test result" AS Matter, Description AS Activity
-- FROM TEST_RESULT
--
-- -- Test recommendations by user
-- UNION
-- SELECT UID, DATE(Time) AS Date, "Test recommendation" AS Matter, Description AS Activity
-- FROM TEST_RECOMMENDATION
--
-- -- Diagnoses by user
-- UNION
-- SELECT UID, DATE(DTimestamp) AS Date, "Diagnosis" AS Matter, DText AS Activity
-- FROM DIAGNOSIS: 0 row(s) affected
-- [62, 20:54:26] DROP VIEW IF EXISTS Home_User_View: Running...
-- [62, 20:54:26] DROP VIEW IF EXISTS Home_User_View: 0 row(s) affected, 1 warning(s):
-- 1051 Unknown table 'test.Home_User_View'
-- [63, 20:54:26] CREATE VIEW Home_User_View AS
-- SELECT UID, Date, Matter, Activity
-- FROM Home_User_View_Activities
-- ORDER BY UID, Date DESC, Matter: Running...
-- [63, 20:54:26] CREATE VIEW Home_User_View AS
-- SELECT UID, Date, Matter, Activity
-- FROM Home_User_View_Activities
-- ORDER BY UID, Date DESC, Matter: 0 row(s) affected
-- [64, 20:54:26] SELECT *
-- FROM Home_User_View
-- WHERE UID = 2
-- LIMIT 0, 1000
-- : Running...
-- [64, 20:54:26] SELECT *
-- FROM Home_User_View
-- WHERE UID = 2
-- LIMIT 0, 1000
-- : Fetching...
-- [64, 20:54:26] SELECT *
-- FROM Home_User_View
-- WHERE UID = 2
-- LIMIT 0, 1000
-- : 11 row(s) returned
-- # UID, Date, Matter, Activity
-- '2', '2016-10-21', 'Answer', 'Anser text 2'
-- '2', '2016-10-21', 'Answer', 'Anser text 6'
-- '2', '2016-10-21', 'Answer', 'Anser text 10'
-- '2', '2016-10-21', 'Diagnosis', 'Diagnosis 4'
-- '2', '2016-10-21', 'Diagnosis', 'Diagnosis 2'
-- '2', '2016-10-21', 'Question', 'Question 2?'
-- '2', '2016-10-21', 'Question', 'Question 6?'
-- '2', '2016-10-21', 'Question', 'Question 7?'
-- '2', '2016-10-21', 'Symptom', 'Description 3'
-- '2', '2016-10-21', 'Test recommendation', 'Test recommendation 6'
-- '2', '2016-10-21', 'Test result', 'Test result description 5'
