

-- Query 4

-- List all clinical opinions for a given home user (diagnosis, recommendation
-- of treatments, or answer questions).

-- Use line 18 to select the user.

SELECT CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type, CLINICAL_OPINIONS.Description
FROM (
      SELECT UID, "Answer" AS Type, AText AS Description
      FROM ANSWER, QUESTION
      WHERE QUESTION.QID = ANSWER.QID
      UNION
      SELECT UID, "Treatment" AS Type, Text AS Description
      FROM TREATMENT
      UNION
      SELECT UID, "Diagnosis" AS Type, DText AS Description
      FROM DIAGNOSIS
     ) AS CLINICAL_OPINIONS
WHERE CLINICAL_OPINIONS.UID = 5
ORDER BY CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type;

-- Output

-- [79, 21:04:40] SELECT CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type, CLINICAL_OPINIONS.Description
-- FROM (
--       SELECT UID, "Answer" AS Type, AText AS Description
--       FROM ANSWER, QUESTION
--       WHERE QUESTION.QID = ANSWER.QID
--       UNION
--       SELECT UID, "Treatment" AS Type, Text AS Description
--       FROM TREATMENT
--       UNION
--       SELECT UID, "Diagnosis" AS Type, DText AS Description
--       FROM DIAGNOSIS
--      ) AS CLINICAL_OPINIONS
-- WHERE CLINICAL_OPINIONS.UID = 5
-- ORDER BY CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type
-- LIMIT 0, 1000
-- : Running...
-- [79, 21:04:40] SELECT CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type, CLINICAL_OPINIONS.Description
-- FROM (
--       SELECT UID, "Answer" AS Type, AText AS Description
--       FROM ANSWER, QUESTION
--       WHERE QUESTION.QID = ANSWER.QID
--       UNION
--       SELECT UID, "Treatment" AS Type, Text AS Description
--       FROM TREATMENT
--       UNION
--       SELECT UID, "Diagnosis" AS Type, DText AS Description
--       FROM DIAGNOSIS
--      ) AS CLINICAL_OPINIONS
-- WHERE CLINICAL_OPINIONS.UID = 5
-- ORDER BY CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type
-- LIMIT 0, 1000
-- : Fetching...
-- [79, 21:04:40] SELECT CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type, CLINICAL_OPINIONS.Description
-- FROM (
--       SELECT UID, "Answer" AS Type, AText AS Description
--       FROM ANSWER, QUESTION
--       WHERE QUESTION.QID = ANSWER.QID
--       UNION
--       SELECT UID, "Treatment" AS Type, Text AS Description
--       FROM TREATMENT
--       UNION
--       SELECT UID, "Diagnosis" AS Type, DText AS Description
--       FROM DIAGNOSIS
--      ) AS CLINICAL_OPINIONS
-- WHERE CLINICAL_OPINIONS.UID = 5
-- ORDER BY CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type
-- LIMIT 0, 1000
-- : 4 row(s) returned
-- # UID, Type, Description
-- '5', 'Answer', 'Anser text 3'
-- '5', 'Answer', 'Anser text 8'
-- '5', 'Diagnosis', 'Diagnosis 5'
-- '5', 'Treatment', 'Treatment text 7'
