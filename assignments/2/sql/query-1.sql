

-- Query 1

-- Add a new user to the system. A user can be either a home user or an online
-- clinician.

-- Replace all <fields between less than and greater than signs> with the user's
-- relevant details.
-- Replace the NULL on line 20 with the OID of the home user's online clinician.

-- Home user
INSERT INTO HOME_USER (Phone, eMail, FName, LName, Address, OID)
VALUES (
    "<phone no>",
    "<email address>",
    "<first name>",
    "<last name>",
    "<address>",
    NULL
);

-- Online clinician
INSERT INTO ONLINE_CLINICIAN (Phone, eMail, FName, LName, Address)
VALUES (
    "<phone no>",
    "<email address>",
    "<first name>",
    "<last name>",
    "<address>"
);

-- Output

-- [75, 21:01:43] INSERT INTO HOME_USER (Phone, eMail, FName, LName, Address, OID)
-- VALUES (
--     "<phone no>",
--     "<email address>",
--     "<first name>",
--     "<last name>",
--     "<address>",
--     NULL
-- ): Running...
-- [75, 21:01:43] INSERT INTO HOME_USER (Phone, eMail, FName, LName, Address, OID)
-- VALUES (
--     "<phone no>",
--     "<email address>",
--     "<first name>",
--     "<last name>",
--     "<address>",
--     NULL
-- ): 1 row(s) affected
-- [76, 21:01:43] INSERT INTO ONLINE_CLINICIAN (Phone, eMail, FName, LName, Address)
-- VALUES (
--     "<phone no>",
--     "<email address>",
--     "<first name>",
--     "<last name>",
--     "<address>"
-- ): Running...
-- [76, 21:01:43] INSERT INTO ONLINE_CLINICIAN (Phone, eMail, FName, LName, Address)
-- VALUES (
--     "<phone no>",
--     "<email address>",
--     "<first name>",
--     "<last name>",
--     "<address>"
-- ): 1 row(s) affected
