

-- Schema construction

-- Independent tables

-- DISEASE (ICD10CODE*, Description)
CREATE TABLE DISEASE  (
    ICD10CODE VARCHAR(8),
    Description TEXT,
    PRIMARY KEY (ICD10CODE)
);

-- HCHT (TESTID*, Condition, Process, Toolname, Method)
CREATE TABLE HCHT (
    TESTID INTEGER AUTO_INCREMENT,
    `Condition` TEXT,
    Process TEXT,
    Toolname TEXT,
    Method TEXT,
    PRIMARY KEY (TESTID)
);

-- ONLINE_CLINICIAN (OID*, Phone, eMail, FName, LName, Address)
CREATE TABLE ONLINE_CLINICIAN (
    OID INTEGER AUTO_INCREMENT,
    Phone VARCHAR(12),
    eMail VARCHAR(64),
    FName VARCHAR(64),
    LName VARCHAR(64),
    Address VARCHAR(128),
    PRIMARY KEY (OID)
);

-- Level 1

-- HOME_USER (UID*, Phone, eMail, FName, LName, Address, OID)
-- HOME_USER.OID -> ONLINE_CLINICIAN.OID
CREATE TABLE HOME_USER (
    UID INTEGER AUTO_INCREMENT,
    Phone VARCHAR(12),
    eMail VARCHAR(64),
    FName VARCHAR(64),
    LName VARCHAR(64),
    Address VARCHAR(128),
    OID INTEGER,
    PRIMARY KEY (UID),
    FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID)
);

-- Level 2

-- CONSULTATION (CID*, UID, OID, Time)
-- CONSULTATION.UID -> HOME_USER.UID
-- CONSULTATION.OID -> ONLINE_CLINICIAN.OID
CREATE TABLE CONSULTATION (
    CID INTEGER AUTO_INCREMENT,
    UID INTEGER,
    OID INTEGER,
    Time TIMESTAMP,
    PRIMARY KEY (CID),
    FOREIGN KEY (UID) REFERENCES HOME_USER (UID),
    FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID)
);

-- DIAGNOSIS (UID*, OID*, DTimestamp*, DText)
-- DIAGNOSIS.UID -> HOME_USER.UID
-- DIAGNOSIS.OID -> ONLINE_CLINICIAN.OID
CREATE TABLE DIAGNOSIS (
    UID INTEGER,
    OID INTEGER,
    DTimestamp TIMESTAMP,
    DText TEXT,
    PRIMARY KEY (UID, OID, DTimestamp),
    FOREIGN KEY (UID) REFERENCES HOME_USER (UID),
    FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID)
);

-- QUESTION (QID*, QText, Type, QTimestamp, UID)
-- QUESTION.UID -> HOME_USER.UID
CREATE TABLE QUESTION (
    QID INTEGER AUTO_INCREMENT,
    QText TEXT,
    Type VARCHAR(8),
    QTimestamp TIMESTAMP,
    UID INTEGER,
    PRIMARY KEY (QID),
    FOREIGN KEY (UID) REFERENCES HOME_USER (UID)
);

-- TEST_RECOMMENDATION (UID*, OID*, Time*, Description, Type, TESTID)
-- TEST_RECOMMENDATION.UID -> HOME_USER.UID
-- TEST_RECOMMENDATION.OID -> ONLINE_CLINICIAN.OID
-- TEST_RECOMMENDATION.TESTID -> HCHT.TESTID
CREATE TABLE TEST_RECOMMENDATION (
    UID INTEGER,
    OID INTEGER,
    Time TIMESTAMP,
    Description TEXT,
    Type TEXT,
    TESTID INTEGER,
    PRIMARY KEY (UID, OID, Time),
    FOREIGN KEY (UID) REFERENCES HOME_USER (UID),
    FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID),
    FOREIGN KEY (TESTID) REFERENCES HCHT (TESTID)
);

-- TEST_RESULT (TRID*, Result, Description, Time, UID, TESTID)
-- TEST_RESULT.UID -> HOME_USER.UID
-- TEST_RESULT.TESTID -> HCHT.TESTID
CREATE TABLE TEST_RESULT (
    TRID INTEGER AUTO_INCREMENT,
    Result TEXT,
    Description TEXT,
    Time TIMESTAMP,
    UID INTEGER,
    TESTID INTEGER,
    PRIMARY KEY (TRID),
    FOREIGN KEY (UID) REFERENCES HOME_USER (UID),
    FOREIGN KEY (TESTID) REFERENCES HCHT (TESTID)
);

-- TREATMENT (TID*, Text, Name, Type, PrefOrder, UID, OID)
-- TREATMENT.OID -> ONLINE_CLINICIAN.OID
-- TREATMENT.UID -> HOME_USER.UID
CREATE TABLE TREATMENT (
    TID INTEGER AUTO_INCREMENT,
    Text TEXT,
    Name TEXT,
    Type TEXT,
    PrefOrder TEXT,
    UID INTEGER,
    OID INTEGER,
    PRIMARY KEY (TID),
    FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID),
    FOREIGN KEY (UID) REFERENCES HOME_USER (UID)
);

-- USER_SIGNS_SYM (UID*, Time*, Severity, Description)
-- USER_SIGNS_SYM.UID -> HOME_USER.UID
CREATE TABLE USER_SIGNS_SYM (
    UID INTEGER,
    Time TIMESTAMP,
    Severity TEXT,
    Description TEXT,
    PRIMARY KEY (UID, Time),
    FOREIGN KEY (UID) REFERENCES HOME_USER (UID)
);

-- Level 3

-- ANSWER (AID*, AText, Type, ATimestamp, QID, OID)
-- ANSWER.QID -> QUESTION.QID
-- ANSWER.OID -> ONLINE_CLINICIAN.OID
CREATE TABLE ANSWER (
    AID INTEGER AUTO_INCREMENT,
    AText TEXT,
    Type VARCHAR(8),
    ATimestamp TIMESTAMP,
    QID INTEGER,
    OID INTEGER,
    PRIMARY KEY (AID),
    FOREIGN KEY (QID) REFERENCES QUESTION (QID),
    FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID)
);

-- DIAG_DISEASE (UID*, OID*, DTimestamp*, ICD10CODE*)
-- DIAG_DISEASE.(UID, OID, DTimestamp) -> DIAGNOSIS.(UID, OID, DTimestamp)
-- DIAG_DISEASE.ICD10CODE -> DISEASE.ICD10CODE
CREATE TABLE DIAG_DISEASE (
    UID INTEGER,
    OID INTEGER,
    DTimestamp TIMESTAMP,
    ICD10CODE VARCHAR(8),
    PRIMARY KEY (UID, OID, DTimestamp, ICD10CODE),
    FOREIGN KEY (UID, OID, DTimestamp) REFERENCES DIAGNOSIS (UID, OID, DTimestamp),
    FOREIGN KEY (ICD10CODE) REFERENCES DISEASE (ICD10CODE)
);

-- Output

-- [32, 20:51:15] CREATE TABLE DISEASE  (
--     ICD10CODE VARCHAR(8),
--     Description TEXT,
--     PRIMARY KEY (ICD10CODE)
-- ): Running...
-- [32, 20:51:15] CREATE TABLE DISEASE  (
--     ICD10CODE VARCHAR(8),
--     Description TEXT,
--     PRIMARY KEY (ICD10CODE)
-- ): 0 row(s) affected
-- [33, 20:51:15] CREATE TABLE HCHT (
--     TESTID INTEGER AUTO_INCREMENT,
--     `Condition` TEXT,
--     Process TEXT,
--     Toolname TEXT,
--     Method TEXT,
--     PRIMARY KEY (TESTID)
-- ): Running...
-- [33, 20:51:15] CREATE TABLE HCHT (
--     TESTID INTEGER AUTO_INCREMENT,
--     `Condition` TEXT,
--     Process TEXT,
--     Toolname TEXT,
--     Method TEXT,
--     PRIMARY KEY (TESTID)
-- ): 0 row(s) affected
-- [34, 20:51:15] CREATE TABLE ONLINE_CLINICIAN (
--     OID INTEGER AUTO_INCREMENT,
--     Phone VARCHAR(12),
--     eMail VARCHAR(64),
--     FName VARCHAR(64),
--     LName VARCHAR(64),
--     Address VARCHAR(128),
--     PRIMARY KEY (OID)
-- ): Running...
-- [34, 20:51:16] CREATE TABLE ONLINE_CLINICIAN (
--     OID INTEGER AUTO_INCREMENT,
--     Phone VARCHAR(12),
--     eMail VARCHAR(64),
--     FName VARCHAR(64),
--     LName VARCHAR(64),
--     Address VARCHAR(128),
--     PRIMARY KEY (OID)
-- ): 0 row(s) affected
-- [35, 20:51:16] CREATE TABLE HOME_USER (
--     UID INTEGER AUTO_INCREMENT,
--     Phone VARCHAR(12),
--     eMail VARCHAR(64),
--     FName VARCHAR(64),
--     LName VARCHAR(64),
--     Address VARCHAR(128),
--     OID INTEGER,
--     PRIMARY KEY (UID),
--     FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID)
-- ): Running...
-- [35, 20:51:16] CREATE TABLE HOME_USER (
--     UID INTEGER AUTO_INCREMENT,
--     Phone VARCHAR(12),
--     eMail VARCHAR(64),
--     FName VARCHAR(64),
--     LName VARCHAR(64),
--     Address VARCHAR(128),
--     OID INTEGER,
--     PRIMARY KEY (UID),
--     FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID)
-- ): 0 row(s) affected
-- [36, 20:51:16] CREATE TABLE CONSULTATION (
--     CID INTEGER AUTO_INCREMENT,
--     UID INTEGER,
--     OID INTEGER,
--     Time TIMESTAMP,
--     PRIMARY KEY (CID),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID),
--     FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID)
-- ): Running...
-- [36, 20:51:16] CREATE TABLE CONSULTATION (
--     CID INTEGER AUTO_INCREMENT,
--     UID INTEGER,
--     OID INTEGER,
--     Time TIMESTAMP,
--     PRIMARY KEY (CID),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID),
--     FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID)
-- ): 0 row(s) affected
-- [37, 20:51:16] CREATE TABLE DIAGNOSIS (
--     UID INTEGER,
--     OID INTEGER,
--     DTimestamp TIMESTAMP,
--     DText TEXT,
--     PRIMARY KEY (UID, OID, DTimestamp),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID),
--     FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID)
-- ): Running...
-- [37, 20:51:16] CREATE TABLE DIAGNOSIS (
--     UID INTEGER,
--     OID INTEGER,
--     DTimestamp TIMESTAMP,
--     DText TEXT,
--     PRIMARY KEY (UID, OID, DTimestamp),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID),
--     FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID)
-- ): 0 row(s) affected
-- [38, 20:51:16] CREATE TABLE QUESTION (
--     QID INTEGER AUTO_INCREMENT,
--     QText TEXT,
--     Type VARCHAR(8),
--     QTimestamp TIMESTAMP,
--     UID INTEGER,
--     PRIMARY KEY (QID),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID)
-- ): Running...
-- [38, 20:51:16] CREATE TABLE QUESTION (
--     QID INTEGER AUTO_INCREMENT,
--     QText TEXT,
--     Type VARCHAR(8),
--     QTimestamp TIMESTAMP,
--     UID INTEGER,
--     PRIMARY KEY (QID),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID)
-- ): 0 row(s) affected
-- [39, 20:51:16] CREATE TABLE TEST_RECOMMENDATION (
--     UID INTEGER,
--     OID INTEGER,
--     Time TIMESTAMP,
--     Description TEXT,
--     Type TEXT,
--     TESTID INTEGER,
--     PRIMARY KEY (UID, OID, Time),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID),
--     FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID),
--     FOREIGN KEY (TESTID) REFERENCES HCHT (TESTID)
-- ): Running...
-- [39, 20:51:16] CREATE TABLE TEST_RECOMMENDATION (
--     UID INTEGER,
--     OID INTEGER,
--     Time TIMESTAMP,
--     Description TEXT,
--     Type TEXT,
--     TESTID INTEGER,
--     PRIMARY KEY (UID, OID, Time),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID),
--     FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID),
--     FOREIGN KEY (TESTID) REFERENCES HCHT (TESTID)
-- ): 0 row(s) affected
-- [40, 20:51:16] CREATE TABLE TEST_RESULT (
--     TRID INTEGER AUTO_INCREMENT,
--     Result TEXT,
--     Description TEXT,
--     Time TIMESTAMP,
--     UID INTEGER,
--     TESTID INTEGER,
--     PRIMARY KEY (TRID),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID),
--     FOREIGN KEY (TESTID) REFERENCES HCHT (TESTID)
-- ): Running...
-- [40, 20:51:16] CREATE TABLE TEST_RESULT (
--     TRID INTEGER AUTO_INCREMENT,
--     Result TEXT,
--     Description TEXT,
--     Time TIMESTAMP,
--     UID INTEGER,
--     TESTID INTEGER,
--     PRIMARY KEY (TRID),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID),
--     FOREIGN KEY (TESTID) REFERENCES HCHT (TESTID)
-- ): 0 row(s) affected
-- [41, 20:51:16] CREATE TABLE TREATMENT (
--     TID INTEGER AUTO_INCREMENT,
--     Text TEXT,
--     Name TEXT,
--     Type TEXT,
--     PrefOrder TEXT,
--     UID INTEGER,
--     OID INTEGER,
--     PRIMARY KEY (TID),
--     FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID)
-- ): Running...
-- [41, 20:51:16] CREATE TABLE TREATMENT (
--     TID INTEGER AUTO_INCREMENT,
--     Text TEXT,
--     Name TEXT,
--     Type TEXT,
--     PrefOrder TEXT,
--     UID INTEGER,
--     OID INTEGER,
--     PRIMARY KEY (TID),
--     FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID)
-- ): 0 row(s) affected
-- [42, 20:51:16] CREATE TABLE USER_SIGNS_SYM (
--     UID INTEGER,
--     Time TIMESTAMP,
--     Severity TEXT,
--     Description TEXT,
--     PRIMARY KEY (UID, Time),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID)
-- ): Running...
-- [42, 20:51:16] CREATE TABLE USER_SIGNS_SYM (
--     UID INTEGER,
--     Time TIMESTAMP,
--     Severity TEXT,
--     Description TEXT,
--     PRIMARY KEY (UID, Time),
--     FOREIGN KEY (UID) REFERENCES HOME_USER (UID)
-- ): 0 row(s) affected
-- [43, 20:51:16] CREATE TABLE ANSWER (
--     AID INTEGER AUTO_INCREMENT,
--     AText TEXT,
--     Type VARCHAR(8),
--     ATimestamp TIMESTAMP,
--     QID INTEGER,
--     OID INTEGER,
--     PRIMARY KEY (AID),
--     FOREIGN KEY (QID) REFERENCES QUESTION (QID),
--     FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID)
-- ): Running...
-- [43, 20:51:16] CREATE TABLE ANSWER (
--     AID INTEGER AUTO_INCREMENT,
--     AText TEXT,
--     Type VARCHAR(8),
--     ATimestamp TIMESTAMP,
--     QID INTEGER,
--     OID INTEGER,
--     PRIMARY KEY (AID),
--     FOREIGN KEY (QID) REFERENCES QUESTION (QID),
--     FOREIGN KEY (OID) REFERENCES ONLINE_CLINICIAN (OID)
-- ): 0 row(s) affected
-- [44, 20:51:16] CREATE TABLE DIAG_DISEASE (
--     UID INTEGER,
--     OID INTEGER,
--     DTimestamp TIMESTAMP,
--     ICD10CODE VARCHAR(8),
--     PRIMARY KEY (UID, OID, DTimestamp, ICD10CODE),
--     FOREIGN KEY (UID, OID, DTimestamp) REFERENCES DIAGNOSIS (UID, OID, DTimestamp),
--     FOREIGN KEY (ICD10CODE) REFERENCES DISEASE (ICD10CODE)
-- ): Running...
-- [44, 20:51:16] CREATE TABLE DIAG_DISEASE (
--     UID INTEGER,
--     OID INTEGER,
--     DTimestamp TIMESTAMP,
--     ICD10CODE VARCHAR(8),
--     PRIMARY KEY (UID, OID, DTimestamp, ICD10CODE),
--     FOREIGN KEY (UID, OID, DTimestamp) REFERENCES DIAGNOSIS (UID, OID, DTimestamp),
--     FOREIGN KEY (ICD10CODE) REFERENCES DISEASE (ICD10CODE)
-- ): 0 row(s) affected


-- Sample data construction

-- # Independent tables

-- * DISEASE (ICD10CODE*, Description)
INSERT INTO DISEASE (ICD10CODE, Description)
VALUES ("L20", "Atopic dermatitis"),
       ("L21", "Seborrhoeic dermatitis"),
       ("L22", "Diaper [napkin] dermatitis"),
       ("L23", "Allergic contact dermatitis"),
       ("L24", "Irritant contact dermatitis"),
       ("L25", "Unspecified contact dermatitis"),
       ("L26", "Exfoliative dermatitis"),
       ("L27", "Dermatitis due to substances taken internally"),
       ("L28", "Lichen simplex chronicus and prurigo"),
       ("L29", "Pruritus");

-- * HCHT (TESTID*, Condition, Process, Toolname, Method)
INSERT INTO HCHT (`Condition`, Process, Toolname, Method)
VALUES ("Condition 1", "Process 1", "Toolname 1", "Method 1"),
       ("Condition 2", "Process 2", "Toolname 2", "Method 2"),
       ("Condition 3", "Process 3", "Toolname 3", "Method 3"),
       ("Condition 4", "Process 4", "Toolname 4", "Method 4"),
       ("Condition 5", "Process 5", "Toolname 5", "Method 5"),
       ("Condition 6", "Process 6", "Toolname 6", "Method 6"),
       ("Condition 7", "Process 7", "Toolname 7", "Method 7"),
       ("Condition 8", "Process 8", "Toolname 8", "Method 8"),
       ("Condition 9", "Process 9", "Toolname 9", "Method 9"),
       ("Condition 10", "Process 10", "Toolname 10", "Method 10");

-- * ONLINE_CLINICIAN (OID*, Phone, eMail, FName, LName, Address)
INSERT INTO ONLINE_CLINICIAN (Phone, eMail, FName, LName, Address)
VALUES ("1234567891", "1@online.clinician", "Dr 1", "MD 1", "Place 1"),
       ("1234567892", "2@online.clinician", "Dr 2", "MD 2", "Place 2"),
       ("1234567893", "3@online.clinician", "Dr 3", "MD 3", "Place 3"),
       ("1234567894", "4@online.clinician", "Dr 4", "MD 4", "Place 4"),
       ("1234567895", "5@online.clinician", "Dr 5", "MD 5", "Place 5"),
       ("1234567896", "6@online.clinician", "Dr 6", "MD 6", "Place 6"),
       ("1234567897", "7@online.clinician", "Dr 7", "MD 7", "Place 7"),
       ("1234567898", "8@online.clinician", "Dr 8", "MD 8", "Place 8"),
       ("1234567899", "9@online.clinician", "Dr 9", "MD 9", "Place 9"),
       ("1234567900", "10@online.clinician", "Dr 10", "MD 10", "Place 10");

-- # Level 1

-- * HOME_USER (UID*, Phone, eMail, FName, LName, Address, OID)
--  * HOME_USER.OID -> ONLINE_CLINICIAN.OID
INSERT INTO HOME_USER (Phone, eMail, FName, LName, Address, OID)
VALUES ("0987654321", "1@home.user", "Real 1", "Person 1", "Home 1", 1),
       ("0987654322", "2@home.user", "Real 2", "Person 2", "Home 2", 7),
       ("0987654323", "3@home.user", "Real 3", "Person 3", "Home 3", 3),
       ("0987654324", "4@home.user", "Real 4", "Person 4", "Home 4", 5),
       ("0987654325", "5@home.user", "Real 5", "Person 5", "Home 5", 5),
       ("0987654326", "6@home.user", "Real 6", "Person 6", "Home 6", 3),
       ("0987654327", "7@home.user", "Real 7", "Person 7", "Home 7", 7),
       ("0987654328", "8@home.user", "Real 8", "Person 8", "Home 8", 10),
       ("0987654329", "9@home.user", "Real 9", "Person 9", "Home 9", 9),
       ("0987654330", "10@home.user", "Real 10", "Person 10", "Home 10", 10);

-- # Level 2

-- * CONSULTATION (CID*, UID, OID, Time)
--  * CONSULTATION.UID -> HOME_USER.UID
--  * CONSULTATION.OID -> ONLINE_CLINICIAN.OID
INSERT INTO CONSULTATION (UID, OID, Time)
VALUES (1, 1, "2016-10-21 01:00:00"),
       (2, 2, "2016-10-21 02:00:00"),
       (3, 3, "2016-10-21 03:00:00"),
       (2, 1, "2016-10-21 04:00:00"),
       (5, 5, "2016-10-21 05:00:00"),
       (6, 7, "2016-10-21 06:00:00"),
       (8, 7, "2016-10-21 07:00:00"),
       (1, 8, "2016-10-21 08:00:00"),
       (9, 3, "2016-10-21 09:00:00"),
       (4, 2, "2016-10-21 10:00:00");

-- * DIAGNOSIS (UID*, OID*, DTimestamp*, DText)
--  * DIAGNOSIS.UID -> HOME_USER.UID
--  * DIAGNOSIS.OID -> ONLINE_CLINICIAN.OID
INSERT INTO DIAGNOSIS (UID, OID, DTimestamp, DText)
VALUES (1, 1, "2016-10-21 01:00:00", "Diagnosis 1"),
       (2, 2, "2016-10-21 02:00:00", "Diagnosis 2"),
       (3, 3, "2016-10-21 03:00:00", "Diagnosis 3"),
       (2, 1, "2016-10-21 04:00:00", "Diagnosis 4"),
       (5, 5, "2016-10-21 05:00:00", "Diagnosis 5"),
       (6, 7, "2016-10-21 06:00:00", "Diagnosis 6"),
       (8, 7, "2016-10-21 07:00:00", "Diagnosis 7"),
       (1, 8, "2016-10-21 08:00:00", "Diagnosis 8"),
       (9, 3, "2016-10-21 09:00:00", "Diagnosis 9"),
       (4, 2, "2016-10-21 10:00:00", "Diagnosis 10");

-- * QUESTION (QID*, QText, Type, QTimestamp, UID)
--  * QUESTION.UID -> HOME_USER.UID
INSERT INTO QUESTION (QText, Type, QTimestamp, UID)
VALUES ("Question 1?", "private", "2016-10-21 01:00:00", 1),
       ("Question 2?", "open", "2016-10-21 02:00:00", 2),
       ("Question 3?", "open", "2016-10-21 03:00:00", 5),
       ("Question 4?", "open", "2016-10-21 04:00:00", 9),
       ("Question 5?", "private", "2016-10-21 05:00:00", 1),
       ("Question 6?", "private", "2016-10-21 06:00:00", 2),
       ("Question 7?", "open", "2016-10-21 07:00:00", 2),
       ("Question 8?", "private", "2016-10-21 08:00:00", 3),
       ("Question 9?", "private", "2016-10-21 09:00:00", 4),
       ("Question 10?", "private", "2016-10-21 10:00:00", 5);

-- * TEST_RECOMMENDATION (UID*, OID*, Time*, Description, Type, TESTID)
--  * TEST_RECOMMENDATION.UID -> HOME_USER.UID
--  * TEST_RECOMMENDATION.OID -> ONLINE_CLINICIAN.OID
--  * TEST_RECOMMENDATION.TESTID -> HCHT.TESTID
INSERT INTO TEST_RECOMMENDATION (UID, OID, Time, Description, Type, TESTID)
VALUES (1, 7, "2016-10-21 01:00:00", "Test recommendation 1", "Type 1", 5),
       (3, 8, "2016-10-21 02:00:00", "Test recommendation 2", "Type 2", 2),
       (7, 6, "2016-10-21 03:00:00", "Test recommendation 3", "Type 3", 4),
       (4, 1, "2016-10-21 04:00:00", "Test recommendation 4", "Type 4", 6),
       (4, 3, "2016-10-21 05:00:00", "Test recommendation 5", "Type 5", 3),
       (2, 4, "2016-10-21 06:00:00", "Test recommendation 6", "Type 6", 5),
       (8, 8, "2016-10-21 07:00:00", "Test recommendation 7", "Type 7", 7),
       (8, 1, "2016-10-21 08:00:00", "Test recommendation 8", "Type 8", 9),
       (9, 3, "2016-10-21 09:00:00", "Test recommendation 9", "Type 9", 4),
       (10, 4, "2016-10-21 10:00:00", "Test recommendation 10", "Type 10", 5);

-- * TEST_RESULT (TRID*, Result, Description, Time, UID, TESTID)
--  * TEST_RESULT.UID -> HOME_USER.UID
--  * TEST_RESULT.TESTID -> HCHT.TESTID
INSERT INTO TEST_RESULT (Result, Description, Time, UID, TESTID)
VALUES ("Test result 1", "Test result description 1", "2016-10-21 01:00:00", 6, 4),
       ("Test result 2", "Test result description 2", "2016-10-21 02:00:00", 3, 6),
       ("Test result 3", "Test result description 3", "2016-10-21 03:00:00", 4, 3),
       ("Test result 4", "Test result description 4", "2016-10-21 04:00:00", 9, 4),
       ("Test result 5", "Test result description 5", "2016-10-21 05:00:00", 2, 9),
       ("Test result 6", "Test result description 6", "2016-10-21 06:00:00", 7, 8),
       ("Test result 7", "Test result description 7", "2016-10-21 07:00:00", 8, 5),
       ("Test result 8", "Test result description 8", "2016-10-21 08:00:00", 1, 1),
       ("Test result 9", "Test result description 9", "2016-10-21 09:00:00", 5, 2),
       ("Test result 10", "Test result description 10", "2016-10-21 10:00:00", 6, 3);

-- * TREATMENT (TID*, Text, Name, Type, PrefOrder, UID, OID)
--  * TREATMENT.OID -> ONLINE_CLINICIAN.OID
--  * TREATMENT.UID -> HOME_USER.UID
INSERT INTO TREATMENT (Text, Name, Type, PrefOrder, UID, OID)
VALUES ("Treatment text 1", "Treatment name 1", "Treatment type 1", "Pref-Order 1", 3, 8),
       ("Treatment text 2", "Treatment name 2", "Treatment type 2", "Pref-Order 2", 4, 4),
       ("Treatment text 3", "Treatment name 3", "Treatment type 3", "Pref-Order 3", 6, 6),
       ("Treatment text 4", "Treatment name 4", "Treatment type 4", "Pref-Order 4", 7, 4),
       ("Treatment text 5", "Treatment name 5", "Treatment type 5", "Pref-Order 5", 9, 6),
       ("Treatment text 6", "Treatment name 6", "Treatment type 6", "Pref-Order 6", 6, 5),
       ("Treatment text 7", "Treatment name 7", "Treatment type 7", "Pref-Order 7", 5, 9),
       ("Treatment text 8", "Treatment name 8", "Treatment type 8", "Pref-Order 8", 3, 8),
       ("Treatment text 9", "Treatment name 9", "Treatment type 9", "Pref-Order 9", 7, 7),
       ("Treatment text 10", "Treatment name 10", "Treatment type 10", "Pref-Order 10", 1, 4);

-- * USER_SIGNS_SYM (UID*, Time*, Severity, Description)
--  * USER_SIGNS_SYM.UID -> HOME_USER.UID
INSERT INTO USER_SIGNS_SYM (UID, Time, Severity, Description)
VALUES (6, "2016-10-21 01:00:00", "Severity 1", "Description 1"),
       (4, "2016-10-21 02:00:00", "Severity 2", "Description 2"),
       (2, "2016-10-21 03:00:00", "Severity 3", "Description 3"),
       (4, "2016-10-21 04:00:00", "Severity 4", "Description 4"),
       (9, "2016-10-21 05:00:00", "Severity 5", "Description 5"),
       (8, "2016-10-21 06:00:00", "Severity 6", "Description 6"),
       (9, "2016-10-21 07:00:00", "Severity 7", "Description 7"),
       (6, "2016-10-21 08:00:00", "Severity 8", "Description 8"),
       (7, "2016-10-21 09:00:00", "Severity 9", "Description 9"),
       (6, "2016-10-21 10:00:00", "Severity 10", "Description 10");

-- # Level 3

-- * ANSWER (AID*, AText, Type, ATimestamp, QID, OID)
--  * ANSWER.QID -> QUESTION.QID
--  * ANSWER.OID -> ONLINE_CLINICIAN.OID
INSERT INTO ANSWER (AText, Type, ATimestamp, QID, OID)
VALUES ("Anser text 1", "private", "2016-10-21 01:00:00", 4, 6),
       ("Anser text 2", "private", "2016-10-21 02:00:00", 2, 2),
       ("Anser text 3", "private", "2016-10-21 03:00:00", 3, 3),
       ("Anser text 4", "private", "2016-10-21 04:00:00", 9, 9),
       ("Anser text 5", "private", "2016-10-21 05:00:00", 4, 6),
       ("Anser text 6", "private", "2016-10-21 06:00:00", 2, 4),
       ("Anser text 7", "private", "2016-10-21 07:00:00", 1, 3),
       ("Anser text 8", "private", "2016-10-21 08:00:00", 3, 7),
       ("Anser text 9", "private", "2016-10-21 09:00:00", 5, 3),
       ("Anser text 10", "private", "2016-10-21 10:00:00", 6, 4);
UPDATE ANSWER
SET Type = (SELECT QUESTION.Type
            FROM QUESTION
            WHERE QUESTION.QID = ANSWER.QID);

-- * DIAG_DISEASE (UID*, OID*, DTimestamp*, ICD10CODE*)
--  * DIAG_DISEASE.(UID, OID, DTimestamp) -> DIAGNOSIS.(UID, OID, DTimestamp)
--  * DIAG_DISEASE.ICD10CODE -> DISEASE.ICD10CODE
INSERT INTO DIAG_DISEASE (UID, OID, DTimestamp, ICD10CODE)
VALUES (1, 1, "2016-10-21 01:00:00", "L20"),
       (2, 2, "2016-10-21 02:00:00", "L21"),
       (3, 3, "2016-10-21 03:00:00", "L22"),
       (2, 1, "2016-10-21 04:00:00", "L23"),
       (5, 5, "2016-10-21 05:00:00", "L24"),
       (6, 7, "2016-10-21 06:00:00", "L25"),
       (1, 8, "2016-10-21 08:00:00", "L26"),
       (8, 7, "2016-10-21 07:00:00", "L27"),
       (9, 3, "2016-10-21 09:00:00", "L28"),
       (4, 2, "2016-10-21 10:00:00", "L29");

-- Output

-- [45, 20:52:40] INSERT INTO DISEASE (ICD10CODE, Description)
-- VALUES ("L20", "Atopic dermatitis"),
--        ("L21", "Seborrhoeic dermatitis"),
--        ("L22", "Diaper [napkin] dermatitis"),
--        ("L23", "Allergic contact dermatitis"),
--        ("L24", "Irritant contact dermatitis"),
--        ("L25", "Unspecified contact dermatitis"),
--        ("L26", "Exfoliative dermatitis"),
--        ("L27", "Dermatitis due to substances taken internally"),
--        ("L28", "Lichen simplex chronicus and prurigo"),
--        ("L29", "Pruritus"): Running...
-- [45, 20:52:40] INSERT INTO DISEASE (ICD10CODE, Description)
-- VALUES ("L20", "Atopic dermatitis"),
--        ("L21", "Seborrhoeic dermatitis"),
--        ("L22", "Diaper [napkin] dermatitis"),
--        ("L23", "Allergic contact dermatitis"),
--        ("L24", "Irritant contact dermatitis"),
--        ("L25", "Unspecified contact dermatitis"),
--        ("L26", "Exfoliative dermatitis"),
--        ("L27", "Dermatitis due to substances taken internally"),
--        ("L28", "Lichen simplex chronicus and prurigo"),
--        ("L29", "Pruritus"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [46, 20:52:40] INSERT INTO HCHT (`Condition`, Process, Toolname, Method)
-- VALUES ("Condition 1", "Process 1", "Toolname 1", "Method 1"),
--        ("Condition 2", "Process 2", "Toolname 2", "Method 2"),
--        ("Condition 3", "Process 3", "Toolname 3", "Method 3"),
--        ("Condition 4", "Process 4", "Toolname 4", "Method 4"),
--        ("Condition 5", "Process 5", "Toolname 5", "Method 5"),
--        ("Condition 6", "Process 6", "Toolname 6", "Method 6"),
--        ("Condition 7", "Process 7", "Toolname 7", "Method 7"),
--        ("Condition 8", "Process 8", "Toolname 8", "Method 8"),
--        ("Condition 9", "Process 9", "Toolname 9", "Method 9"),
--        ("Condition 10", "Process 10", "Toolname 10", "Method 10"): Running...
-- [46, 20:52:40] INSERT INTO HCHT (`Condition`, Process, Toolname, Method)
-- VALUES ("Condition 1", "Process 1", "Toolname 1", "Method 1"),
--        ("Condition 2", "Process 2", "Toolname 2", "Method 2"),
--        ("Condition 3", "Process 3", "Toolname 3", "Method 3"),
--        ("Condition 4", "Process 4", "Toolname 4", "Method 4"),
--        ("Condition 5", "Process 5", "Toolname 5", "Method 5"),
--        ("Condition 6", "Process 6", "Toolname 6", "Method 6"),
--        ("Condition 7", "Process 7", "Toolname 7", "Method 7"),
--        ("Condition 8", "Process 8", "Toolname 8", "Method 8"),
--        ("Condition 9", "Process 9", "Toolname 9", "Method 9"),
--        ("Condition 10", "Process 10", "Toolname 10", "Method 10"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [47, 20:52:40] INSERT INTO ONLINE_CLINICIAN (Phone, eMail, FName, LName, Address)
-- VALUES ("1234567891", "1@online.clinician", "Dr 1", "MD 1", "Place 1"),
--        ("1234567892", "2@online.clinician", "Dr 2", "MD 2", "Place 2"),
--        ("1234567893", "3@online.clinician", "Dr 3", "MD 3", "Place 3"),
--        ("1234567894", "4@online.clinician", "Dr 4", "MD 4", "Place 4"),
--        ("1234567895", "5@online.clinician", "Dr 5", "MD 5", "Place 5"),
--        ("1234567896", "6@online.clinician", "Dr 6", "MD 6", "Place 6"),
--        ("1234567897", "7@online.clinician", "Dr 7", "MD 7", "Place 7"),
--        ("1234567898", "8@online.clinician", "Dr 8", "MD 8", "Place 8"),
--        ("1234567899", "9@online.clinician", "Dr 9", "MD 9", "Place 9"),
--        ("1234567900", "10@online.clinician", "Dr 10", "MD 10", "Place 10"): Running...
-- [47, 20:52:40] INSERT INTO ONLINE_CLINICIAN (Phone, eMail, FName, LName, Address)
-- VALUES ("1234567891", "1@online.clinician", "Dr 1", "MD 1", "Place 1"),
--        ("1234567892", "2@online.clinician", "Dr 2", "MD 2", "Place 2"),
--        ("1234567893", "3@online.clinician", "Dr 3", "MD 3", "Place 3"),
--        ("1234567894", "4@online.clinician", "Dr 4", "MD 4", "Place 4"),
--        ("1234567895", "5@online.clinician", "Dr 5", "MD 5", "Place 5"),
--        ("1234567896", "6@online.clinician", "Dr 6", "MD 6", "Place 6"),
--        ("1234567897", "7@online.clinician", "Dr 7", "MD 7", "Place 7"),
--        ("1234567898", "8@online.clinician", "Dr 8", "MD 8", "Place 8"),
--        ("1234567899", "9@online.clinician", "Dr 9", "MD 9", "Place 9"),
--        ("1234567900", "10@online.clinician", "Dr 10", "MD 10", "Place 10"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [48, 20:52:40] INSERT INTO HOME_USER (Phone, eMail, FName, LName, Address, OID)
-- VALUES ("0987654321", "1@home.user", "Real 1", "Person 1", "Home 1", 1),
--        ("0987654322", "2@home.user", "Real 2", "Person 2", "Home 2", 7),
--        ("0987654323", "3@home.user", "Real 3", "Person 3", "Home 3", 3),
--        ("0987654324", "4@home.user", "Real 4", "Person 4", "Home 4", 5),
--        ("0987654325", "5@home.user", "Real 5", "Person 5", "Home 5", 5),
--        ("0987654326", "6@home.user", "Real 6", "Person 6", "Home 6", 3),
--        ("0987654327", "7@home.user", "Real 7", "Person 7", "Home 7", 7),
--        ("0987654328", "8@home.user", "Real 8", "Person 8", "Home 8", 10),
--        ("0987654329", "9@home.user", "Real 9", "Person 9", "Home 9", 9),
--        ("0987654330", "10@home.user", "Real 10", "Person 10", "Home 10", 10): Running...
-- [48, 20:52:40] INSERT INTO HOME_USER (Phone, eMail, FName, LName, Address, OID)
-- VALUES ("0987654321", "1@home.user", "Real 1", "Person 1", "Home 1", 1),
--        ("0987654322", "2@home.user", "Real 2", "Person 2", "Home 2", 7),
--        ("0987654323", "3@home.user", "Real 3", "Person 3", "Home 3", 3),
--        ("0987654324", "4@home.user", "Real 4", "Person 4", "Home 4", 5),
--        ("0987654325", "5@home.user", "Real 5", "Person 5", "Home 5", 5),
--        ("0987654326", "6@home.user", "Real 6", "Person 6", "Home 6", 3),
--        ("0987654327", "7@home.user", "Real 7", "Person 7", "Home 7", 7),
--        ("0987654328", "8@home.user", "Real 8", "Person 8", "Home 8", 10),
--        ("0987654329", "9@home.user", "Real 9", "Person 9", "Home 9", 9),
--        ("0987654330", "10@home.user", "Real 10", "Person 10", "Home 10", 10): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [49, 20:52:40] INSERT INTO CONSULTATION (UID, OID, Time)
-- VALUES (1, 1, "2016-10-21 01:00:00"),
--        (2, 2, "2016-10-21 02:00:00"),
--        (3, 3, "2016-10-21 03:00:00"),
--        (2, 1, "2016-10-21 04:00:00"),
--        (5, 5, "2016-10-21 05:00:00"),
--        (6, 7, "2016-10-21 06:00:00"),
--        (8, 7, "2016-10-21 07:00:00"),
--        (1, 8, "2016-10-21 08:00:00"),
--        (9, 3, "2016-10-21 09:00:00"),
--        (4, 2, "2016-10-21 10:00:00"): Running...
-- [49, 20:52:40] INSERT INTO CONSULTATION (UID, OID, Time)
-- VALUES (1, 1, "2016-10-21 01:00:00"),
--        (2, 2, "2016-10-21 02:00:00"),
--        (3, 3, "2016-10-21 03:00:00"),
--        (2, 1, "2016-10-21 04:00:00"),
--        (5, 5, "2016-10-21 05:00:00"),
--        (6, 7, "2016-10-21 06:00:00"),
--        (8, 7, "2016-10-21 07:00:00"),
--        (1, 8, "2016-10-21 08:00:00"),
--        (9, 3, "2016-10-21 09:00:00"),
--        (4, 2, "2016-10-21 10:00:00"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [50, 20:52:40] INSERT INTO DIAGNOSIS (UID, OID, DTimestamp, DText)
-- VALUES (1, 1, "2016-10-21 01:00:00", "Diagnosis 1"),
--        (2, 2, "2016-10-21 02:00:00", "Diagnosis 2"),
--        (3, 3, "2016-10-21 03:00:00", "Diagnosis 3"),
--        (2, 1, "2016-10-21 04:00:00", "Diagnosis 4"),
--        (5, 5, "2016-10-21 05:00:00", "Diagnosis 5"),
--        (6, 7, "2016-10-21 06:00:00", "Diagnosis 6"),
--        (8, 7, "2016-10-21 07:00:00", "Diagnosis 7"),
--        (1, 8, "2016-10-21 08:00:00", "Diagnosis 8"),
--        (9, 3, "2016-10-21 09:00:00", "Diagnosis 9"),
--        (4, 2, "2016-10-21 10:00:00", "Diagnosis 10"): Running...
-- [50, 20:52:40] INSERT INTO DIAGNOSIS (UID, OID, DTimestamp, DText)
-- VALUES (1, 1, "2016-10-21 01:00:00", "Diagnosis 1"),
--        (2, 2, "2016-10-21 02:00:00", "Diagnosis 2"),
--        (3, 3, "2016-10-21 03:00:00", "Diagnosis 3"),
--        (2, 1, "2016-10-21 04:00:00", "Diagnosis 4"),
--        (5, 5, "2016-10-21 05:00:00", "Diagnosis 5"),
--        (6, 7, "2016-10-21 06:00:00", "Diagnosis 6"),
--        (8, 7, "2016-10-21 07:00:00", "Diagnosis 7"),
--        (1, 8, "2016-10-21 08:00:00", "Diagnosis 8"),
--        (9, 3, "2016-10-21 09:00:00", "Diagnosis 9"),
--        (4, 2, "2016-10-21 10:00:00", "Diagnosis 10"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [51, 20:52:40] INSERT INTO QUESTION (QText, Type, QTimestamp, UID)
-- VALUES ("Question 1?", "private", "2016-10-21 01:00:00", 1),
--        ("Question 2?", "open", "2016-10-21 02:00:00", 2),
--        ("Question 3?", "open", "2016-10-21 03:00:00", 5),
--        ("Question 4?", "open", "2016-10-21 04:00:00", 9),
--        ("Question 5?", "private", "2016-10-21 05:00:00", 1),
--        ("Question 6?", "private", "2016-10-21 06:00:00", 2),
--        ("Question 7?", "open", "2016-10-21 07:00:00", 2),
--        ("Question 8?", "private", "2016-10-21 08:00:00", 3),
--        ("Question 9?", "private", "2016-10-21 09:00:00", 4),
--        ("Question 10?", "private", "2016-10-21 10:00:00", 5): Running...
-- [51, 20:52:40] INSERT INTO QUESTION (QText, Type, QTimestamp, UID)
-- VALUES ("Question 1?", "private", "2016-10-21 01:00:00", 1),
--        ("Question 2?", "open", "2016-10-21 02:00:00", 2),
--        ("Question 3?", "open", "2016-10-21 03:00:00", 5),
--        ("Question 4?", "open", "2016-10-21 04:00:00", 9),
--        ("Question 5?", "private", "2016-10-21 05:00:00", 1),
--        ("Question 6?", "private", "2016-10-21 06:00:00", 2),
--        ("Question 7?", "open", "2016-10-21 07:00:00", 2),
--        ("Question 8?", "private", "2016-10-21 08:00:00", 3),
--        ("Question 9?", "private", "2016-10-21 09:00:00", 4),
--        ("Question 10?", "private", "2016-10-21 10:00:00", 5): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [52, 20:52:40] INSERT INTO TEST_RECOMMENDATION (UID, OID, Time, Description, Type, TESTID)
-- VALUES (1, 7, "2016-10-21 01:00:00", "Test recommendation 1", "Type 1", 5),
--        (3, 8, "2016-10-21 02:00:00", "Test recommendation 2", "Type 2", 2),
--        (7, 6, "2016-10-21 03:00:00", "Test recommendation 3", "Type 3", 4),
--        (4, 1, "2016-10-21 04:00:00", "Test recommendation 4", "Type 4", 6),
--        (4, 3, "2016-10-21 05:00:00", "Test recommendation 5", "Type 5", 3),
--        (2, 4, "2016-10-21 06:00:00", "Test recommendation 6", "Type 6", 5),
--        (8, 8, "2016-10-21 07:00:00", "Test recommendation 7", "Type 7", 7),
--        (8, 1, "2016-10-21 08:00:00", "Test recommendation 8", "Type 8", 9),
--        (9, 3, "2016-10-21 09:00:00", "Test recommendation 9", "Type 9", 4),
--        (10, 4, "2016-10-21 10:00:00", "Test recommendation 10", "Type 10", 5): Running...
-- [52, 20:52:40] INSERT INTO TEST_RECOMMENDATION (UID, OID, Time, Description, Type, TESTID)
-- VALUES (1, 7, "2016-10-21 01:00:00", "Test recommendation 1", "Type 1", 5),
--        (3, 8, "2016-10-21 02:00:00", "Test recommendation 2", "Type 2", 2),
--        (7, 6, "2016-10-21 03:00:00", "Test recommendation 3", "Type 3", 4),
--        (4, 1, "2016-10-21 04:00:00", "Test recommendation 4", "Type 4", 6),
--        (4, 3, "2016-10-21 05:00:00", "Test recommendation 5", "Type 5", 3),
--        (2, 4, "2016-10-21 06:00:00", "Test recommendation 6", "Type 6", 5),
--        (8, 8, "2016-10-21 07:00:00", "Test recommendation 7", "Type 7", 7),
--        (8, 1, "2016-10-21 08:00:00", "Test recommendation 8", "Type 8", 9),
--        (9, 3, "2016-10-21 09:00:00", "Test recommendation 9", "Type 9", 4),
--        (10, 4, "2016-10-21 10:00:00", "Test recommendation 10", "Type 10", 5): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [53, 20:52:40] INSERT INTO TEST_RESULT (Result, Description, Time, UID, TESTID)
-- VALUES ("Test result 1", "Test result description 1", "2016-10-21 01:00:00", 6, 4),
--        ("Test result 2", "Test result description 2", "2016-10-21 02:00:00", 3, 6),
--        ("Test result 3", "Test result description 3", "2016-10-21 03:00:00", 4, 3),
--        ("Test result 4", "Test result description 4", "2016-10-21 04:00:00", 9, 4),
--        ("Test result 5", "Test result description 5", "2016-10-21 05:00:00", 2, 9),
--        ("Test result 6", "Test result description 6", "2016-10-21 06:00:00", 7, 8),
--        ("Test result 7", "Test result description 7", "2016-10-21 07:00:00", 8, 5),
--        ("Test result 8", "Test result description 8", "2016-10-21 08:00:00", 1, 1),
--        ("Test result 9", "Test result description 9", "2016-10-21 09:00:00", 5, 2),
--        ("Test result 10", "Test result description 10", "2016-10-21 10:00:00", 6, 3): Running...
-- [53, 20:52:40] INSERT INTO TEST_RESULT (Result, Description, Time, UID, TESTID)
-- VALUES ("Test result 1", "Test result description 1", "2016-10-21 01:00:00", 6, 4),
--        ("Test result 2", "Test result description 2", "2016-10-21 02:00:00", 3, 6),
--        ("Test result 3", "Test result description 3", "2016-10-21 03:00:00", 4, 3),
--        ("Test result 4", "Test result description 4", "2016-10-21 04:00:00", 9, 4),
--        ("Test result 5", "Test result description 5", "2016-10-21 05:00:00", 2, 9),
--        ("Test result 6", "Test result description 6", "2016-10-21 06:00:00", 7, 8),
--        ("Test result 7", "Test result description 7", "2016-10-21 07:00:00", 8, 5),
--        ("Test result 8", "Test result description 8", "2016-10-21 08:00:00", 1, 1),
--        ("Test result 9", "Test result description 9", "2016-10-21 09:00:00", 5, 2),
--        ("Test result 10", "Test result description 10", "2016-10-21 10:00:00", 6, 3): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [54, 20:52:40] INSERT INTO TREATMENT (Text, Name, Type, PrefOrder, UID, OID)
-- VALUES ("Treatment text 1", "Treatment name 1", "Treatment type 1", "Pref-Order 1", 3, 8),
--        ("Treatment text 2", "Treatment name 2", "Treatment type 2", "Pref-Order 2", 4, 4),
--        ("Treatment text 3", "Treatment name 3", "Treatment type 3", "Pref-Order 3", 6, 6),
--        ("Treatment text 4", "Treatment name 4", "Treatment type 4", "Pref-Order 4", 7, 4),
--        ("Treatment text 5", "Treatment name 5", "Treatment type 5", "Pref-Order 5", 9, 6),
--        ("Treatment text 6", "Treatment name 6", "Treatment type 6", "Pref-Order 6", 6, 5),
--        ("Treatment text 7", "Treatment name 7", "Treatment type 7", "Pref-Order 7", 5, 9),
--        ("Treatment text 8", "Treatment name 8", "Treatment type 8", "Pref-Order 8", 3, 8),
--        ("Treatment text 9", "Treatment name 9", "Treatment type 9", "Pref-Order 9", 7, 7),
--        ("Treatment text 10", "Treatment name 10", "Treatment type 10", "Pref-Order 10", 1, 4): Running...
-- [54, 20:52:40] INSERT INTO TREATMENT (Text, Name, Type, PrefOrder, UID, OID)
-- VALUES ("Treatment text 1", "Treatment name 1", "Treatment type 1", "Pref-Order 1", 3, 8),
--        ("Treatment text 2", "Treatment name 2", "Treatment type 2", "Pref-Order 2", 4, 4),
--        ("Treatment text 3", "Treatment name 3", "Treatment type 3", "Pref-Order 3", 6, 6),
--        ("Treatment text 4", "Treatment name 4", "Treatment type 4", "Pref-Order 4", 7, 4),
--        ("Treatment text 5", "Treatment name 5", "Treatment type 5", "Pref-Order 5", 9, 6),
--        ("Treatment text 6", "Treatment name 6", "Treatment type 6", "Pref-Order 6", 6, 5),
--        ("Treatment text 7", "Treatment name 7", "Treatment type 7", "Pref-Order 7", 5, 9),
--        ("Treatment text 8", "Treatment name 8", "Treatment type 8", "Pref-Order 8", 3, 8),
--        ("Treatment text 9", "Treatment name 9", "Treatment type 9", "Pref-Order 9", 7, 7),
--        ("Treatment text 10", "Treatment name 10", "Treatment type 10", "Pref-Order 10", 1, 4): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [55, 20:52:40] INSERT INTO USER_SIGNS_SYM (UID, Time, Severity, Description)
-- VALUES (6, "2016-10-21 01:00:00", "Severity 1", "Description 1"),
--        (4, "2016-10-21 02:00:00", "Severity 2", "Description 2"),
--        (2, "2016-10-21 03:00:00", "Severity 3", "Description 3"),
--        (4, "2016-10-21 04:00:00", "Severity 4", "Description 4"),
--        (9, "2016-10-21 05:00:00", "Severity 5", "Description 5"),
--        (8, "2016-10-21 06:00:00", "Severity 6", "Description 6"),
--        (9, "2016-10-21 07:00:00", "Severity 7", "Description 7"),
--        (6, "2016-10-21 08:00:00", "Severity 8", "Description 8"),
--        (7, "2016-10-21 09:00:00", "Severity 9", "Description 9"),
--        (6, "2016-10-21 10:00:00", "Severity 10", "Description 10"): Running...
-- [55, 20:52:40] INSERT INTO USER_SIGNS_SYM (UID, Time, Severity, Description)
-- VALUES (6, "2016-10-21 01:00:00", "Severity 1", "Description 1"),
--        (4, "2016-10-21 02:00:00", "Severity 2", "Description 2"),
--        (2, "2016-10-21 03:00:00", "Severity 3", "Description 3"),
--        (4, "2016-10-21 04:00:00", "Severity 4", "Description 4"),
--        (9, "2016-10-21 05:00:00", "Severity 5", "Description 5"),
--        (8, "2016-10-21 06:00:00", "Severity 6", "Description 6"),
--        (9, "2016-10-21 07:00:00", "Severity 7", "Description 7"),
--        (6, "2016-10-21 08:00:00", "Severity 8", "Description 8"),
--        (7, "2016-10-21 09:00:00", "Severity 9", "Description 9"),
--        (6, "2016-10-21 10:00:00", "Severity 10", "Description 10"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [56, 20:52:40] INSERT INTO ANSWER (AText, Type, ATimestamp, QID, OID)
-- VALUES ("Anser text 1", "private", "2016-10-21 01:00:00", 4, 6),
--        ("Anser text 2", "private", "2016-10-21 02:00:00", 2, 2),
--        ("Anser text 3", "private", "2016-10-21 03:00:00", 3, 3),
--        ("Anser text 4", "private", "2016-10-21 04:00:00", 9, 9),
--        ("Anser text 5", "private", "2016-10-21 05:00:00", 4, 6),
--        ("Anser text 6", "private", "2016-10-21 06:00:00", 2, 4),
--        ("Anser text 7", "private", "2016-10-21 07:00:00", 1, 3),
--        ("Anser text 8", "private", "2016-10-21 08:00:00", 3, 7),
--        ("Anser text 9", "private", "2016-10-21 09:00:00", 5, 3),
--        ("Anser text 10", "private", "2016-10-21 10:00:00", 6, 4): Running...
-- [56, 20:52:40] INSERT INTO ANSWER (AText, Type, ATimestamp, QID, OID)
-- VALUES ("Anser text 1", "private", "2016-10-21 01:00:00", 4, 6),
--        ("Anser text 2", "private", "2016-10-21 02:00:00", 2, 2),
--        ("Anser text 3", "private", "2016-10-21 03:00:00", 3, 3),
--        ("Anser text 4", "private", "2016-10-21 04:00:00", 9, 9),
--        ("Anser text 5", "private", "2016-10-21 05:00:00", 4, 6),
--        ("Anser text 6", "private", "2016-10-21 06:00:00", 2, 4),
--        ("Anser text 7", "private", "2016-10-21 07:00:00", 1, 3),
--        ("Anser text 8", "private", "2016-10-21 08:00:00", 3, 7),
--        ("Anser text 9", "private", "2016-10-21 09:00:00", 5, 3),
--        ("Anser text 10", "private", "2016-10-21 10:00:00", 6, 4): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0
-- [57, 20:52:40] UPDATE ANSWER
-- SET Type = (SELECT QUESTION.Type
--             FROM QUESTION
--             WHERE QUESTION.QID = ANSWER.QID): Running...
-- [57, 20:52:40] UPDATE ANSWER
-- SET Type = (SELECT QUESTION.Type
--             FROM QUESTION
--             WHERE QUESTION.QID = ANSWER.QID): 6 row(s) affected
-- Rows matched: 10  Changed: 6  Warnings: 0
-- [58, 20:52:40] INSERT INTO DIAG_DISEASE (UID, OID, DTimestamp, ICD10CODE)
-- VALUES (1, 1, "2016-10-21 01:00:00", "L20"),
--        (2, 2, "2016-10-21 02:00:00", "L21"),
--        (3, 3, "2016-10-21 03:00:00", "L22"),
--        (2, 1, "2016-10-21 04:00:00", "L23"),
--        (5, 5, "2016-10-21 05:00:00", "L24"),
--        (6, 7, "2016-10-21 06:00:00", "L25"),
--        (1, 8, "2016-10-21 08:00:00", "L26"),
--        (8, 7, "2016-10-21 07:00:00", "L27"),
--        (9, 3, "2016-10-21 09:00:00", "L28"),
--        (4, 2, "2016-10-21 10:00:00", "L29"): Running...
-- [58, 20:52:40] INSERT INTO DIAG_DISEASE (UID, OID, DTimestamp, ICD10CODE)
-- VALUES (1, 1, "2016-10-21 01:00:00", "L20"),
--        (2, 2, "2016-10-21 02:00:00", "L21"),
--        (3, 3, "2016-10-21 03:00:00", "L22"),
--        (2, 1, "2016-10-21 04:00:00", "L23"),
--        (5, 5, "2016-10-21 05:00:00", "L24"),
--        (6, 7, "2016-10-21 06:00:00", "L25"),
--        (1, 8, "2016-10-21 08:00:00", "L26"),
--        (8, 7, "2016-10-21 07:00:00", "L27"),
--        (9, 3, "2016-10-21 09:00:00", "L28"),
--        (4, 2, "2016-10-21 10:00:00", "L29"): 10 row(s) affected
-- Records: 10  Duplicates: 0  Warnings: 0


-- Query 1

-- Add a new user to the system. A user can be either a home user or an online
-- clinician.

-- Replace all <fields between less than and greater than signs> with the user's
-- relevant details.
-- Replace the NULL on line 20 with the OID of the home user's online clinician.

-- Home user
INSERT INTO HOME_USER (Phone, eMail, FName, LName, Address, OID)
VALUES (
    "<phone no>",
    "<email address>",
    "<first name>",
    "<last name>",
    "<address>",
    NULL
);

-- Online clinician
INSERT INTO ONLINE_CLINICIAN (Phone, eMail, FName, LName, Address)
VALUES (
    "<phone no>",
    "<email address>",
    "<first name>",
    "<last name>",
    "<address>"
);

-- Output

-- [75, 21:01:43] INSERT INTO HOME_USER (Phone, eMail, FName, LName, Address, OID)
-- VALUES (
--     "<phone no>",
--     "<email address>",
--     "<first name>",
--     "<last name>",
--     "<address>",
--     NULL
-- ): Running...
-- [75, 21:01:43] INSERT INTO HOME_USER (Phone, eMail, FName, LName, Address, OID)
-- VALUES (
--     "<phone no>",
--     "<email address>",
--     "<first name>",
--     "<last name>",
--     "<address>",
--     NULL
-- ): 1 row(s) affected
-- [76, 21:01:43] INSERT INTO ONLINE_CLINICIAN (Phone, eMail, FName, LName, Address)
-- VALUES (
--     "<phone no>",
--     "<email address>",
--     "<first name>",
--     "<last name>",
--     "<address>"
-- ): Running...
-- [76, 21:01:43] INSERT INTO ONLINE_CLINICIAN (Phone, eMail, FName, LName, Address)
-- VALUES (
--     "<phone no>",
--     "<email address>",
--     "<first name>",
--     "<last name>",
--     "<address>"
-- ): 1 row(s) affected


-- Query 2

-- List healthcare consultation requests (symptoms, questions, diagnostic test
-- results) for home users.

SELECT CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type, CONSULTATION_REQUESTS.Description
FROM (
      SELECT UID, "Question" AS Type, QText AS Description
      FROM QUESTION
      UNION
      SELECT UID, "Symptom" AS Type, Description AS Description
      FROM USER_SIGNS_SYM
      UNION
      SELECT UID, "Test result" AS Type, Description AS Description
      FROM TEST_RESULT
     ) AS CONSULTATION_REQUESTS
ORDER BY CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type;

-- Output

-- [77, 21:02:17] SELECT CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type, CONSULTATION_REQUESTS.Description
-- FROM (
--       SELECT UID, "Question" AS Type, QText AS Description
--       FROM QUESTION
--       UNION
--       SELECT UID, "Symptom" AS Type, Description AS Description
--       FROM USER_SIGNS_SYM
--       UNION
--       SELECT UID, "Test result" AS Type, Description AS Description
--       FROM TEST_RESULT
--      ) AS CONSULTATION_REQUESTS
-- ORDER BY CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type
-- LIMIT 0, 1000
-- : Running...
-- [77, 21:02:17] SELECT CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type, CONSULTATION_REQUESTS.Description
-- FROM (
--       SELECT UID, "Question" AS Type, QText AS Description
--       FROM QUESTION
--       UNION
--       SELECT UID, "Symptom" AS Type, Description AS Description
--       FROM USER_SIGNS_SYM
--       UNION
--       SELECT UID, "Test result" AS Type, Description AS Description
--       FROM TEST_RESULT
--      ) AS CONSULTATION_REQUESTS
-- ORDER BY CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type
-- LIMIT 0, 1000
-- : Fetching...
-- [77, 21:02:17] SELECT CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type, CONSULTATION_REQUESTS.Description
-- FROM (
--       SELECT UID, "Question" AS Type, QText AS Description
--       FROM QUESTION
--       UNION
--       SELECT UID, "Symptom" AS Type, Description AS Description
--       FROM USER_SIGNS_SYM
--       UNION
--       SELECT UID, "Test result" AS Type, Description AS Description
--       FROM TEST_RESULT
--      ) AS CONSULTATION_REQUESTS
-- ORDER BY CONSULTATION_REQUESTS.UID, CONSULTATION_REQUESTS.Type
-- LIMIT 0, 1000
-- : 30 row(s) returned
-- # UID, Type, Description
-- '1', 'Question', 'Question 1?'
-- '1', 'Question', 'Question 5?'
-- '1', 'Test result', 'Test result description 8'
-- '2', 'Question', 'Question 2?'
-- '2', 'Question', 'Question 6?'
-- '2', 'Question', 'Question 7?'
-- '2', 'Symptom', 'Description 3'
-- '2', 'Test result', 'Test result description 5'
-- '3', 'Question', 'Question 8?'
-- '3', 'Test result', 'Test result description 2'
-- '4', 'Question', 'Question 9?'
-- '4', 'Symptom', 'Description 2'
-- '4', 'Symptom', 'Description 4'
-- '4', 'Test result', 'Test result description 3'
-- '5', 'Question', 'Question 3?'
-- '5', 'Question', 'Question 10?'
-- '5', 'Test result', 'Test result description 9'
-- '6', 'Symptom', 'Description 1'
-- '6', 'Symptom', 'Description 8'
-- '6', 'Symptom', 'Description 10'
-- '6', 'Test result', 'Test result description 1'
-- '6', 'Test result', 'Test result description 10'
-- '7', 'Symptom', 'Description 9'
-- '7', 'Test result', 'Test result description 6'
-- '8', 'Symptom', 'Description 6'
-- '8', 'Test result', 'Test result description 7'
-- '9', 'Question', 'Question 4?'
-- '9', 'Symptom', 'Description 5'
-- '9', 'Symptom', 'Description 7'
-- '9', 'Test result', 'Test result description 4'


-- Query 3

-- List home users for an online clinician. For a given online clinician, all
-- home users should be retrieved from the database.

-- Use lines 13-15 to enter the online clinician details (comment out unknowns)

SELECT HOME_USER.Phone, HOME_USER.eMail, HOME_USER.FName, HOME_USER.LName, HOME_USER.Address
FROM ONLINE_CLINICIAN, HOME_USER
WHERE HOME_USER.OID = ONLINE_CLINICIAN.OID
AND ONLINE_CLINICIAN.OID = 3
-- AND ONLINE_CLINICIAN.FName = "Dr 3"
-- AND ONLINE_CLINICIAN.LName = "MD 3"
ORDER BY ONLINE_CLINICIAN.OID;

-- Output

-- [78, 21:03:49] SELECT HOME_USER.Phone, HOME_USER.eMail, HOME_USER.FName, HOME_USER.LName, HOME_USER.Address
-- FROM ONLINE_CLINICIAN, HOME_USER
-- WHERE HOME_USER.OID = ONLINE_CLINICIAN.OID
-- AND ONLINE_CLINICIAN.OID = 3
-- -- AND ONLINE_CLINICIAN.FName = "Dr 3"
-- -- AND ONLINE_CLINICIAN.LName = "MD 3"
-- ORDER BY ONLINE_CLINICIAN.OID
-- LIMIT 0, 1000
-- : Running...
-- [78, 21:03:49] SELECT HOME_USER.Phone, HOME_USER.eMail, HOME_USER.FName, HOME_USER.LName, HOME_USER.Address
-- FROM ONLINE_CLINICIAN, HOME_USER
-- WHERE HOME_USER.OID = ONLINE_CLINICIAN.OID
-- AND ONLINE_CLINICIAN.OID = 3
-- -- AND ONLINE_CLINICIAN.FName = "Dr 3"
-- -- AND ONLINE_CLINICIAN.LName = "MD 3"
-- ORDER BY ONLINE_CLINICIAN.OID
-- LIMIT 0, 1000
-- : Fetching...
-- [78, 21:03:49] SELECT HOME_USER.Phone, HOME_USER.eMail, HOME_USER.FName, HOME_USER.LName, HOME_USER.Address
-- FROM ONLINE_CLINICIAN, HOME_USER
-- WHERE HOME_USER.OID = ONLINE_CLINICIAN.OID
-- AND ONLINE_CLINICIAN.OID = 3
-- -- AND ONLINE_CLINICIAN.FName = "Dr 3"
-- -- AND ONLINE_CLINICIAN.LName = "MD 3"
-- ORDER BY ONLINE_CLINICIAN.OID
-- LIMIT 0, 1000
-- : 2 row(s) returned
-- # Phone, eMail, FName, LName, Address
-- '0987654323', '3@home.user', 'Real 3', 'Person 3', 'Home 3'
-- '0987654326', '6@home.user', 'Real 6', 'Person 6', 'Home 6'


-- Query 4

-- List all clinical opinions for a given home user (diagnosis, recommendation
-- of treatments, or answer questions).

-- Use line 18 to select the user.

SELECT CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type, CLINICAL_OPINIONS.Description
FROM (
      SELECT UID, "Answer" AS Type, AText AS Description
      FROM ANSWER, QUESTION
      WHERE QUESTION.QID = ANSWER.QID
      UNION
      SELECT UID, "Treatment" AS Type, Text AS Description
      FROM TREATMENT
      UNION
      SELECT UID, "Diagnosis" AS Type, DText AS Description
      FROM DIAGNOSIS
     ) AS CLINICAL_OPINIONS
WHERE CLINICAL_OPINIONS.UID = 5
ORDER BY CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type;

-- Output

-- [79, 21:04:40] SELECT CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type, CLINICAL_OPINIONS.Description
-- FROM (
--       SELECT UID, "Answer" AS Type, AText AS Description
--       FROM ANSWER, QUESTION
--       WHERE QUESTION.QID = ANSWER.QID
--       UNION
--       SELECT UID, "Treatment" AS Type, Text AS Description
--       FROM TREATMENT
--       UNION
--       SELECT UID, "Diagnosis" AS Type, DText AS Description
--       FROM DIAGNOSIS
--      ) AS CLINICAL_OPINIONS
-- WHERE CLINICAL_OPINIONS.UID = 5
-- ORDER BY CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type
-- LIMIT 0, 1000
-- : Running...
-- [79, 21:04:40] SELECT CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type, CLINICAL_OPINIONS.Description
-- FROM (
--       SELECT UID, "Answer" AS Type, AText AS Description
--       FROM ANSWER, QUESTION
--       WHERE QUESTION.QID = ANSWER.QID
--       UNION
--       SELECT UID, "Treatment" AS Type, Text AS Description
--       FROM TREATMENT
--       UNION
--       SELECT UID, "Diagnosis" AS Type, DText AS Description
--       FROM DIAGNOSIS
--      ) AS CLINICAL_OPINIONS
-- WHERE CLINICAL_OPINIONS.UID = 5
-- ORDER BY CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type
-- LIMIT 0, 1000
-- : Fetching...
-- [79, 21:04:40] SELECT CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type, CLINICAL_OPINIONS.Description
-- FROM (
--       SELECT UID, "Answer" AS Type, AText AS Description
--       FROM ANSWER, QUESTION
--       WHERE QUESTION.QID = ANSWER.QID
--       UNION
--       SELECT UID, "Treatment" AS Type, Text AS Description
--       FROM TREATMENT
--       UNION
--       SELECT UID, "Diagnosis" AS Type, DText AS Description
--       FROM DIAGNOSIS
--      ) AS CLINICAL_OPINIONS
-- WHERE CLINICAL_OPINIONS.UID = 5
-- ORDER BY CLINICAL_OPINIONS.UID, CLINICAL_OPINIONS.Type
-- LIMIT 0, 1000
-- : 4 row(s) returned
-- # UID, Type, Description
-- '5', 'Answer', 'Anser text 3'
-- '5', 'Answer', 'Anser text 8'
-- '5', 'Diagnosis', 'Diagnosis 5'
-- '5', 'Treatment', 'Treatment text 7'


-- Query 5

-- Update a user record in the system. A user can be either a home user or an
-- online clinician.

-- Replace all <fields between less than and greater than signs> with the user's
-- relevant details.
-- Replace the NULL on line 23 with the OID of the home user's online clinician.

-- Select the user to update with the values on lines 25-27 and 39-41, comment
-- out unknowns.

-- Home user
UPDATE HOME_USER
SET
    Phone = "<phone no>",
    eMail = "<email address>",
    FName = "<first name>",
    LName = "<last name>",
    Address = "<address>",
    OID = NULL
WHERE 1
-- AND FName = "<first name>"
-- AND LName = "<first name>"
AND UID = 2
;

-- Online clinician
UPDATE ONLINE_CLINICIAN
SET
    Phone = "<phone no>",
    eMail = "<email address>",
    FName = "<first name>",
    LName = "<last name>",
    Address = "<address>"
WHERE 1
-- AND FName = "<first name>"
-- AND LName = "<first name>"
AND OID = 2
;

-- Output

-- [82, 21:07:08] UPDATE HOME_USER
-- SET
--     Phone = "<phone no>",
--     eMail = "<email address>",
--     FName = "<first name>",
--     LName = "<last name>",
--     Address = "<address>",
--     OID = NULL
-- WHERE 1
-- -- AND FName = "<first name>"
-- -- AND LName = "<first name>"
-- AND UID = 2: Running...
-- [82, 21:07:08] UPDATE HOME_USER
-- SET
--     Phone = "<phone no>",
--     eMail = "<email address>",
--     FName = "<first name>",
--     LName = "<last name>",
--     Address = "<address>",
--     OID = NULL
-- WHERE 1
-- -- AND FName = "<first name>"
-- -- AND LName = "<first name>"
-- AND UID = 2: 1 row(s) affected
-- Rows matched: 1  Changed: 1  Warnings: 0
-- [83, 21:07:08] UPDATE ONLINE_CLINICIAN
-- SET
--     Phone = "<phone no>",
--     eMail = "<email address>",
--     FName = "<first name>",
--     LName = "<last name>",
--     Address = "<address>"
-- WHERE 1
-- -- AND FName = "<first name>"
-- -- AND LName = "<first name>"
-- AND OID = 2: Running...
-- [83, 21:07:08] UPDATE ONLINE_CLINICIAN
-- SET
--     Phone = "<phone no>",
--     eMail = "<email address>",
--     FName = "<first name>",
--     LName = "<last name>",
--     Address = "<address>"
-- WHERE 1
-- -- AND FName = "<first name>"
-- -- AND LName = "<first name>"
-- AND OID = 2: 1 row(s) affected
-- Rows matched: 1  Changed: 1  Warnings: 0


-- Query 6

-- Remove a clinician and records associated with this clinician in all tables
-- from the system. You need to decide the strategy on how the Foreign Key
-- values are to be handled in the system. For example, you can replace the
-- removed Foreign Key values by NULL values (called Nullified strategy).

-- Use nullified strategy

-- ONLINE_CLINICIAN
--  DIAG_DISEASE.OID (primary key, delete rows)
--  DIAGNOSIS.OID (primary key, delete rows)
--  TEST_RECOMMENDATION.OID (primary key, delete rows)
--  ANSWER.OID
--  CONSULTATION.OID
--  HOME_USER.OID
--  TREATMENT.OID

-- Set remove_oid to the OID of the online clinician to be removed
SET @remove_oid = 3;

-- DIAG_DISEASE
DELETE FROM DIAG_DISEASE
WHERE OID = @remove_oid;

-- DIAGNOSIS
DELETE FROM DIAGNOSIS
WHERE OID = @remove_oid;

-- TEST_RECOMMENDATION
DELETE FROM TEST_RECOMMENDATION
WHERE OID = @remove_oid;

-- ANSWER
UPDATE ANSWER
SET OID = NULL
WHERE OID = @remove_oid;

-- CONSULTATION
UPDATE CONSULTATION
SET OID = NULL
WHERE OID = @remove_oid;

-- HOME_USER
UPDATE HOME_USER
SET OID = NULL
WHERE OID = @remove_oid;

-- TREATMENT
UPDATE TREATMENT
SET OID = NULL
WHERE OID = @remove_oid;

-- ONLINE_CLINICIAN
DELETE FROM ONLINE_CLINICIAN
WHERE OID = @remove_oid;

-- Output

-- [84, 21:08:32] SET @remove_oid = 3: Running...
-- [84, 21:08:32] SET @remove_oid = 3: 0 row(s) affected
-- [85, 21:08:32] DELETE FROM DIAG_DISEASE
-- WHERE OID = @remove_oid: Running...
-- [85, 21:08:32] DELETE FROM DIAG_DISEASE
-- WHERE OID = @remove_oid: 2 row(s) affected
-- [86, 21:08:32] DELETE FROM DIAGNOSIS
-- WHERE OID = @remove_oid: Running...
-- [86, 21:08:32] DELETE FROM DIAGNOSIS
-- WHERE OID = @remove_oid: 2 row(s) affected
-- [87, 21:08:32] DELETE FROM TEST_RECOMMENDATION
-- WHERE OID = @remove_oid: Running...
-- [87, 21:08:32] DELETE FROM TEST_RECOMMENDATION
-- WHERE OID = @remove_oid: 2 row(s) affected
-- [88, 21:08:32] UPDATE ANSWER
-- SET OID = NULL
-- WHERE OID = @remove_oid: Running...
-- [88, 21:08:32] UPDATE ANSWER
-- SET OID = NULL
-- WHERE OID = @remove_oid: 3 row(s) affected
-- Rows matched: 3  Changed: 3  Warnings: 0
-- [89, 21:08:32] UPDATE CONSULTATION
-- SET OID = NULL
-- WHERE OID = @remove_oid: Running...
-- [89, 21:08:32] UPDATE CONSULTATION
-- SET OID = NULL
-- WHERE OID = @remove_oid: 2 row(s) affected
-- Rows matched: 2  Changed: 2  Warnings: 0
-- [90, 21:08:32] UPDATE HOME_USER
-- SET OID = NULL
-- WHERE OID = @remove_oid: Running...
-- [90, 21:08:32] UPDATE HOME_USER
-- SET OID = NULL
-- WHERE OID = @remove_oid: 2 row(s) affected
-- Rows matched: 2  Changed: 2  Warnings: 0
-- [91, 21:08:32] UPDATE TREATMENT
-- SET OID = NULL
-- WHERE OID = @remove_oid: Running...
-- [91, 21:08:32] UPDATE TREATMENT
-- SET OID = NULL
-- WHERE OID = @remove_oid: 0 row(s) affected
-- Rows matched: 0  Changed: 0  Warnings: 0
-- [92, 21:08:32] DELETE FROM ONLINE_CLINICIAN
-- WHERE OID = @remove_oid: Running...
-- [92, 21:08:32] DELETE FROM ONLINE_CLINICIAN
-- WHERE OID = @remove_oid: 1 row(s) affected


-- View 1

-- Home User View:
-- This view is to be generated for every home user. List the activities of
-- healthcare consultations. The listing should appear in the order of date,
-- and then by matters that the user consulted (symptoms, questions).

-- Assumptions:
-- "healthcare consultations" are questions, answers, symptoms, test results,
-- test recommendations and diagnoses

-- Variables:
-- Adjust the home user this view is for by changing UID on line 57.

-- View for activities
DROP VIEW IF EXISTS Home_User_View_Activities;
CREATE VIEW Home_User_View_Activities AS

-- Question by user
SELECT UID, DATE(QTimestamp) AS Date, "Question" AS Matter, QText AS Activity
FROM QUESTION

-- Answer by user
UNION
SELECT UID, DATE(ATimestamp) AS Date, "Answer" AS Matter, AText AS Activity
FROM ANSWER, QUESTION
WHERE QUESTION.QID = ANSWER.QID

-- Symptoms by user
UNION
SELECT UID, DATE(Time) AS Date, "Symptom" AS Matter, Description AS Activity
FROM USER_SIGNS_SYM

-- Test results by user
UNION
SELECT UID, DATE(Time) AS Date, "Test result" AS Matter, Description AS Activity
FROM TEST_RESULT

-- Test recommendations by user
UNION
SELECT UID, DATE(Time) AS Date, "Test recommendation" AS Matter, Description AS Activity
FROM TEST_RECOMMENDATION

-- Diagnoses by user
UNION
SELECT UID, DATE(DTimestamp) AS Date, "Diagnosis" AS Matter, DText AS Activity
FROM DIAGNOSIS;

-- View proper
DROP VIEW IF EXISTS Home_User_View;
CREATE VIEW Home_User_View AS
SELECT UID, Date, Matter, Activity
FROM Home_User_View_Activities
ORDER BY UID, Date DESC, Matter;

-- Query to get view for a specific home user
SELECT *
FROM Home_User_View
WHERE UID = 2;

-- Output

-- [60, 20:54:26] DROP VIEW IF EXISTS Home_User_View_Activities: Running...
-- [60, 20:54:26] DROP VIEW IF EXISTS Home_User_View_Activities: 0 row(s) affected, 1 warning(s):
-- 1051 Unknown table 'test.Home_User_View_Activities'
-- [61, 20:54:26] CREATE VIEW Home_User_View_Activities AS
--
-- -- Question by user
-- SELECT UID, DATE(QTimestamp) AS Date, "Question" AS Matter, QText AS Activity
-- FROM QUESTION
--
-- -- Answer by user
-- UNION
-- SELECT UID, DATE(ATimestamp) AS Date, "Answer" AS Matter, AText AS Activity
-- FROM ANSWER, QUESTION
-- WHERE QUESTION.QID = ANSWER.QID
--
-- -- Symptoms by user
-- UNION
-- SELECT UID, DATE(Time) AS Date, "Symptom" AS Matter, Description AS Activity
-- FROM USER_SIGNS_SYM
--
-- -- Test results by user
-- UNION
-- SELECT UID, DATE(Time) AS Date, "Test result" AS Matter, Description AS Activity
-- FROM TEST_RESULT
--
-- -- Test recommendations by user
-- UNION
-- SELECT UID, DATE(Time) AS Date, "Test recommendation" AS Matter, Description AS Activity
-- FROM TEST_RECOMMENDATION
--
-- -- Diagnoses by user
-- UNION
-- SELECT UID, DATE(DTimestamp) AS Date, "Diagnosis" AS Matter, DText AS Activity
-- FROM DIAGNOSIS: Running...
-- [61, 20:54:26] CREATE VIEW Home_User_View_Activities AS
--
-- -- Question by user
-- SELECT UID, DATE(QTimestamp) AS Date, "Question" AS Matter, QText AS Activity
-- FROM QUESTION
--
-- -- Answer by user
-- UNION
-- SELECT UID, DATE(ATimestamp) AS Date, "Answer" AS Matter, AText AS Activity
-- FROM ANSWER, QUESTION
-- WHERE QUESTION.QID = ANSWER.QID
--
-- -- Symptoms by user
-- UNION
-- SELECT UID, DATE(Time) AS Date, "Symptom" AS Matter, Description AS Activity
-- FROM USER_SIGNS_SYM
--
-- -- Test results by user
-- UNION
-- SELECT UID, DATE(Time) AS Date, "Test result" AS Matter, Description AS Activity
-- FROM TEST_RESULT
--
-- -- Test recommendations by user
-- UNION
-- SELECT UID, DATE(Time) AS Date, "Test recommendation" AS Matter, Description AS Activity
-- FROM TEST_RECOMMENDATION
--
-- -- Diagnoses by user
-- UNION
-- SELECT UID, DATE(DTimestamp) AS Date, "Diagnosis" AS Matter, DText AS Activity
-- FROM DIAGNOSIS: 0 row(s) affected
-- [62, 20:54:26] DROP VIEW IF EXISTS Home_User_View: Running...
-- [62, 20:54:26] DROP VIEW IF EXISTS Home_User_View: 0 row(s) affected, 1 warning(s):
-- 1051 Unknown table 'test.Home_User_View'
-- [63, 20:54:26] CREATE VIEW Home_User_View AS
-- SELECT UID, Date, Matter, Activity
-- FROM Home_User_View_Activities
-- ORDER BY UID, Date DESC, Matter: Running...
-- [63, 20:54:26] CREATE VIEW Home_User_View AS
-- SELECT UID, Date, Matter, Activity
-- FROM Home_User_View_Activities
-- ORDER BY UID, Date DESC, Matter: 0 row(s) affected
-- [64, 20:54:26] SELECT *
-- FROM Home_User_View
-- WHERE UID = 2
-- LIMIT 0, 1000
-- : Running...
-- [64, 20:54:26] SELECT *
-- FROM Home_User_View
-- WHERE UID = 2
-- LIMIT 0, 1000
-- : Fetching...
-- [64, 20:54:26] SELECT *
-- FROM Home_User_View
-- WHERE UID = 2
-- LIMIT 0, 1000
-- : 11 row(s) returned
-- # UID, Date, Matter, Activity
-- '2', '2016-10-21', 'Answer', 'Anser text 2'
-- '2', '2016-10-21', 'Answer', 'Anser text 6'
-- '2', '2016-10-21', 'Answer', 'Anser text 10'
-- '2', '2016-10-21', 'Diagnosis', 'Diagnosis 4'
-- '2', '2016-10-21', 'Diagnosis', 'Diagnosis 2'
-- '2', '2016-10-21', 'Question', 'Question 2?'
-- '2', '2016-10-21', 'Question', 'Question 6?'
-- '2', '2016-10-21', 'Question', 'Question 7?'
-- '2', '2016-10-21', 'Symptom', 'Description 3'
-- '2', '2016-10-21', 'Test recommendation', 'Test recommendation 6'
-- '2', '2016-10-21', 'Test result', 'Test result description 5'


-- View 2

-- Clinician Summary View:
-- For every online clinician, generate a list of his/her home users. Show all
-- details of home users who relate to the clinician. Provide also a count of
-- home users per clinician as summary.

-- Variables:
-- Adjust the online clinician this view is for by changing OID on line 27.

-- View for home user counts
DROP VIEW IF EXISTS Clinician_Home_User_Counts;
CREATE VIEW Clinician_Home_User_Counts AS
SELECT OID, COUNT(*) AS HomeUserCount
FROM HOME_USER
GROUP BY OID;

-- View for all online clinicians
DROP VIEW IF EXISTS Clinician_Summary_View;
CREATE VIEW Clinician_Summary_View AS
SELECT HOME_USER.OID, HomeUserCount, Phone, eMail, FName, LName, Address
FROM HOME_USER, Clinician_Home_User_Counts
WHERE Clinician_Home_User_Counts.OID = HOME_USER.OID
ORDER BY HOME_USER.OID;

-- Filter view for just one online clinician
SELECT *
FROM Clinician_Summary_View
WHERE OID = 3;

-- Output

-- [65, 20:56:21] DROP VIEW IF EXISTS Clinician_Home_User_Counts: Running...
-- [65, 20:56:21] DROP VIEW IF EXISTS Clinician_Home_User_Counts: 0 row(s) affected, 1 warning(s):
-- 1051 Unknown table 'test.Clinician_Home_User_Counts'
-- [66, 20:56:21] CREATE VIEW Clinician_Home_User_Counts AS
-- SELECT OID, COUNT(*) AS HomeUserCount
-- FROM HOME_USER
-- GROUP BY OID: Running...
-- [66, 20:56:21] CREATE VIEW Clinician_Home_User_Counts AS
-- SELECT OID, COUNT(*) AS HomeUserCount
-- FROM HOME_USER
-- GROUP BY OID: 0 row(s) affected
-- [67, 20:56:21] DROP VIEW IF EXISTS Clinician_Summary_View: Running...
-- [67, 20:56:21] DROP VIEW IF EXISTS Clinician_Summary_View: 0 row(s) affected, 1 warning(s):
-- 1051 Unknown table 'test.Clinician_Summary_View'
-- [68, 20:56:21] CREATE VIEW Clinician_Summary_View AS
-- SELECT HOME_USER.OID, HomeUserCount, Phone, eMail, FName, LName, Address
-- FROM HOME_USER, Clinician_Home_User_Counts
-- WHERE Clinician_Home_User_Counts.OID = HOME_USER.OID
-- ORDER BY HOME_USER.OID: Running...
-- [68, 20:56:21] CREATE VIEW Clinician_Summary_View AS
-- SELECT HOME_USER.OID, HomeUserCount, Phone, eMail, FName, LName, Address
-- FROM HOME_USER, Clinician_Home_User_Counts
-- WHERE Clinician_Home_User_Counts.OID = HOME_USER.OID
-- ORDER BY HOME_USER.OID: 0 row(s) affected
-- [69, 20:56:21] SELECT *
-- FROM Clinician_Summary_View
-- WHERE OID = 3
-- LIMIT 0, 1000
-- : Running...
-- [69, 20:56:21] SELECT *
-- FROM Clinician_Summary_View
-- WHERE OID = 3
-- LIMIT 0, 1000
-- : Fetching...
-- [69, 20:56:21] SELECT *
-- FROM Clinician_Summary_View
-- WHERE OID = 3
-- LIMIT 0, 1000
-- : 2 row(s) returned
-- # OID, HomeUserCount, Phone, eMail, FName, LName, Address
-- '3', '2', '0987654323', '3@home.user', 'Real 3', 'Person 3', 'Home 3'
-- '3', '2', '0987654326', '6@home.user', 'Real 6', 'Person 6', 'Home 6'


-- View 3

-- Consultation View:
-- Generate a view that lists all the questions and answers asked by home users
-- and answered by online clinicians. The view should firstly sort on the dates
-- of questions asked, then sort on the questions.

DROP VIEW IF EXISTS Consultation_View;
CREATE VIEW Consultation_View AS
SELECT DATE(QTimestamp) AS Date, QText, Atext
FROM QUESTION, ANSWER
WHERE ANSWER.QID = QUESTION.QID
ORDER BY Date DESC, QText;

SELECT *
FROM Consultation_View;

-- Output

-- [70, 20:57:15] DROP VIEW IF EXISTS Consultation_View: Running...
-- [70, 20:57:15] DROP VIEW IF EXISTS Consultation_View: 0 row(s) affected, 1 warning(s):
-- 1051 Unknown table 'test.Consultation_View'
-- [71, 20:57:15] CREATE VIEW Consultation_View AS
-- SELECT DATE(QTimestamp) AS Date, QText, Atext
-- FROM QUESTION, ANSWER
-- WHERE ANSWER.QID = QUESTION.QID
-- ORDER BY Date DESC, QText: Running...
-- [71, 20:57:15] CREATE VIEW Consultation_View AS
-- SELECT DATE(QTimestamp) AS Date, QText, Atext
-- FROM QUESTION, ANSWER
-- WHERE ANSWER.QID = QUESTION.QID
-- ORDER BY Date DESC, QText: 0 row(s) affected
-- [72, 20:57:15] SELECT *
-- FROM Consultation_View
-- LIMIT 0, 1000
-- : Running...
-- [72, 20:57:15] SELECT *
-- FROM Consultation_View
-- LIMIT 0, 1000
-- : Fetching...
-- [72, 20:57:15] SELECT *
-- FROM Consultation_View
-- LIMIT 0, 1000
-- : 10 row(s) returned
-- # Date, QText, Atext
-- '2016-10-21', 'Question 1?', 'Anser text 7'
-- '2016-10-21', 'Question 2?', 'Anser text 2'
-- '2016-10-21', 'Question 2?', 'Anser text 6'
-- '2016-10-21', 'Question 3?', 'Anser text 3'
-- '2016-10-21', 'Question 3?', 'Anser text 8'
-- '2016-10-21', 'Question 4?', 'Anser text 1'
-- '2016-10-21', 'Question 4?', 'Anser text 5'
-- '2016-10-21', 'Question 5?', 'Anser text 9'
-- '2016-10-21', 'Question 6?', 'Anser text 10'
-- '2016-10-21', 'Question 9?', 'Anser text 4'
